# Offline Location Service

A HTTP location service with Mozilla Location Service (MLS) compatible API.
Takes a list of observed wifi access points and / or mobile cell ids
and returns a latitude and longitude using various data sources.  Not a web
service, but a device-local server with caching databases and the aim of
minimizing network access and information leakage.  Designed to be run on Linux
with [GeoClue](https://gitlab.freedesktop.org/geoclue/geoclue) but may also
work on other platforms.

## Requirements

Python 3.11 or later (for asyncio cancel scopes). See `requirements.txt` for
library requirements.

## Data sources

Currently OLS can use data from the following sources:

 * Local database collected with a GPS-enabled device and Geoclue running with
   data submission enabled.
 * [Wigle.net](https://wigle.net) wifi and cell location backend with a local
   database cache and offline location resolution for cache hits. API keys are
required, the free API keys unfortunately allow only a low number of daily
queries.
 * Pass-through web backend to Ichnaea-compatible geolocation API (former MLS,
   BeaconDB, Google, Combain etc.). These may need API keys.
 * Offline cell id database with data from the likes of
   [opencellid.org](https://opencellid.org/) and MLS cell ID database (not
updated or available from Mozilla any more)
 * Offline [M8B file](https://github.com/wiglenet/m8binary/releases) from
   wigle.net for wifi location with 1 km accuracy. The m8b file needs to be
converted to msgpack files for the needed [MGRS grid
zones](https://en.wikipedia.org/wiki/Military_Grid_Reference_System#Grid_zone_designation)
with the included `m8b-to-msgpack` script.

## Installation

Packages are currently not available. You can run OLS from the source tree as a
Python module

    python3 -m ols -d debug -C ols.toml

or using the entry point scripts under `bin`

    bin/ols -d debug -C ols.toml

Alternatively, you can make a local install from the source tree with
[pipx](https://pypa.github.io/pipx/)

    pipx install ./

The `ols` executable should then be at `$HOME/.local/bin`.

For permanent use, it's recommended to run OLS as a systemd user unit (or with
[superd](https://git.sr.ht/~craftyguy/superd)). See the example service file
`ols/data/ols-example-user.service` in sources.

## Configuration and usage

Configuration of OLS is currently documented in the [example configuration
file](ols/data/ols-example-conf.toml).

The recommended setup with a GPS-enabled device is to use the clustering locator
with a local database and Geoclue configured to submit data to the local OLS
server. You probably need to scout your usual locations with the GPS source
enabled in Geoclue (by using a map or navigation app) to get an initial database
which works for you. The data is collected every time Geoclue gets a good GPS
location, so your local database expands naturally every time you use a map
application.

If your device does not have GPS (or even if it does, all resolvers in OLS can
be used in parallel), you can use the clustering locator with localdb resolver
in the first group and a passthrough to an web locator (using [BeaconDB](
https://beacondb.net/), for example) as a backup. The local database will be
updated with web locations and will function as cache for subsequent queries.

For fully offline operation without a GPS or cell network receiver (like a
laptop with wifi), the m8b locator using the wigle.net data releases may be of
interest.

### Geoclue configuration

In order to make Geoclue use results from OLS, the configuration file at
`/etc/geoclue/geoclue.conf` needs to be edited to make the wifi source URL point
to the local OLS server. These lines should work with the default port
configuration:

    [wifi]
    enable=true
    url=http://localhost:8088/v1/geolocate

The Geoclue demo application can be now used to test the locator:

    /usr/libexec/geoclue-2.0/demos/where-am-i -a 6

If you are running OLS with debug logging enabled (`-d debug`), OLS should
now output a lot of log messages and the demo app should print your location.

If you have GPS in your device and want to collect a local database, you should
add these lines to the `[wifi]` section:

    submit-data=true
    submission-url=http://localhost:8088/v2/geosubmit

### Cellid data

If you have a device with a ModemManager supported cell modem, GeoClue will
receive cellid data and pass it to the location service, i.e. OLS.

Cellid location data can be downloaded in the opencellid CSV format from
[opencellid.org](https://opencellid.org/) (requires registration).
The former Mozilla Location Service (MLS) also used to host cell-id dumps.

The `cellid-ols-import` command under `bin` can be used to convert the
downloaded CSV files to an sqlite3 database which can be used by the 'cellid'
resolver in OLS. It's very much recommended to use the `--mcc` and `--mnc`
arguments to `cellid-ols-import`, so that the database contains only the cell
ids of your operator.

You can use this command to find the MCC and MNC of the cell which your device
is currently using:

    mmcli -m any --location-enable-3gpp && mmcli -m any --location-get

## See also

 * MicroG
   [Unified Network Location Provider](https://github.com/microg/UnifiedNlp)
