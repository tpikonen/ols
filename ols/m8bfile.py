# Copyright 2022-2024 Teemu Ikonen
# SPDX-License-Identifier: AGPL-3.0-only

import argparse
import bisect
import logging
import mmap
import re
import struct
from collections import OrderedDict, defaultdict
from collections.abc import Iterator, Mapping, Sequence

import indexed_gzip
import msgpack

log = logging.getLogger(__name__)


class CachedMapping(Mapping):
    """Give the given read-only dict without None values an LRU cache."""

    def __init__(self, source, maxsize=1024):
        self.source = source
        self.maxsize = maxsize
        self.cache = OrderedDict()

    def __getitem__(self, key):
        """Get an item by key."""
        try:
            val = self.cache[key]
            del self.cache[key]
            self.cache[key] = val
        except KeyError:
            val = self.source.get(key)
            self.cache[key] = val
            if len(self.cache) > self.maxsize:
                self.cache.popitem(last=False)

        if val is None:
            raise KeyError()

        return val

    def __iter__(self):
        """Return an iterator over the keys."""
        return iter(self.source)

    def __len__(self):
        """Return the number of items in the dictionary."""
        return len(self.source)


class FileBytearray:
    """Adapts a file object to a bytearray-like object."""

    def __init__(self, file):
        self.file = file

    def __getitem__(self, index):
        """Get a byte or slice by index."""
        if isinstance(index, slice):
            self.file.seek(index.start)
            length = index.stop - index.start
            arr = self.file.read(length)
            return arr[0:length:index.step]
        elif isinstance(index, int):
            self.file.seek(index)
            return self.file.read(1)
        else:
            raise TypeError("Index must be a slice or an integer")
        return self.file.read(1)


class M8bFile(Iterator):
    """An Iterator generated from an m8b file from wigle.net."""

    def __init__(self, filename):
        self.initialize(filename)
        # FIXME: 'I' is 4 bytes of input, should be self.idlen bytes
        self.structformat = f'<I{self.coordlen}s'

    def __next__(self):
        """Iterator.__next__ implementation for M8bFile."""
        try:
            buf = self.file.read(self.recordlen)
            netid, mcoords = struct.unpack(self.structformat, buf)
        except struct.error:
            raise StopIteration
        mcoords = mcoords.decode('utf-8')
        return netid, mcoords

    def __len__(self):
        """Return the number of records in the file."""
        return self.records

    def initialize(self, fname):
        if fname.endswith('.gz'):
            self.is_gzip = True
            ff = indexed_gzip.open(fname, mode='rb')
            log.debug("Building index for gzipped m8b file")
            ff.build_full_index()
        else:
            self.is_gzip = False
            ff = open(fname, 'rb')

        def getline(fo):
            return fo.readline().decode('utf-8').strip('\n')

        try:
            self.magic = getline(ff)
            if self.magic != 'MJG':
                raise ValueError(f"Unknown magic bytes: {self.magic[:3]}")

            self.version = int(getline(ff))
            if self.version != 2:
                raise ValueError(f"Unknown file version: {self.version}")

            self.hashfun = getline(ff)
            if self.hashfun != "SIP-2-4":
                raise ValueError(f"Unknown hash function: {self.hashfun}")

            self.slicebits = int(getline(ff), base=16)
            if self.slicebits != 32:
                raise ValueError(f"Surprising number of slicebits: {self.slicebits}")

            self.coordsystem = getline(ff)
            if self.coordsystem != "MGRS-1000":
                raise ValueError(f"Unknown coordinate system: {self.coordsystem}")

            self.idlen = int(getline(ff), base=16)
            if self.idlen != 4:
                raise ValueError(f"Surprising id length (bytes): {self.idlen}")

            self.coordlen = int(getline(ff), base=16)
            if self.coordlen != 9:
                raise ValueError(f"Surprising coordinate length: {self.coordlen}")

            self.records = int(getline(ff), base=16)
        except UnicodeDecodeError:
            ff.close()
            raise ValueError("File is not a valid m8b file")

        self.recordlen = self.idlen + self.coordlen
        self.headerlen = ff.tell()
        self.file = ff


class M8bSequence(M8bFile, Sequence):
    """A Sequence (i.e. indexable) representation of an m8b file from wigle.net."""

    def __init__(self, filename):
        super().__init__(filename)
        if self.is_gzip:
            self.mapped = FileBytearray(self.file)
        else:
            self.mapped = mmap.mmap(self.file.fileno(), 0, access=mmap.ACCESS_READ)

    def __getitem__(self, index):
        """Get a record by index."""
        if index >= self.records or index < 0:
            raise IndexError(f"Index {index} out of range")

        offset = self.headerlen + index * self.recordlen
        netid, mcoords = struct.unpack(
            self.structformat, self.mapped[offset:(offset + self.recordlen)])
        mcoords = mcoords.decode('utf-8')
        return netid, mcoords

    def __iter__(self):
        """Return an iterator over the records."""
        for i in range(self.records):
            yield self[i]


class M8bMapping(Mapping):
    """A Mapping from a clipped hash to a single (first) MGRS coordinate."""

    # FIXME: Does not implement all Mapping methods.

    def __init__(self, filename):
        self.seq = M8bSequence(filename)

    def __next__(self):
        """Iterator.__next__ implementation for M8bMapping."""
        return next(self.seq)

    def __len__(self):
        """Return the number of records in the file."""
        return self.seq.records

    def __iter__(self):
        """Return an iterator over the keys."""
        yield from (netid for netid, _ in self.seq)

    def __getitem__(self, key):
        """Get a record by key."""
        # FIXME: Slow because of bisect
        if not isinstance(key, int):
            raise TypeError("Key must be an integer")

        ind = bisect.bisect_left(self.seq, key, key=lambda x: x[0])
        netid, mcoords = self.seq[ind]
        if netid != key:
            raise KeyError(f"Key {key} not found")

        return mcoords


class M8bSetMapping(M8bMapping):
    """A Mapping from a clipped hash to a list of MGRS coordinates."""

    def __iter__(self):
        """Return an iterator over the keys."""
        prev = -1
        for netid, _ in self.seq:
            if netid != prev:
                yield netid
                prev = netid

    def __getitem__(self, key):
        """Get a record by key."""
        # FIXME: Slow because of bisect
        log.debug(f"Getting key {key}")
        if not isinstance(key, int):
            raise TypeError("Key must be an integer")

        ind = bisect.bisect_left(self.seq, key, key=lambda x: x[0])
        netid, mcoords = self.seq[ind]
        if netid != key:
            raise KeyError(f"Key {key} not found")

        coords = [mcoords]
        while True:
            ind += 1
            try:
                netid, mcoords = self.seq[ind]
            except IndexError:
                break
            if netid != key:
                break
            coords.append(mcoords)

        return coords


def from_file(filename):
    return M8bFile(filename)


def m8b_to_msgpacks(fname, zones=[]):
    if not zones:
        print("Writing files for all MGRS grid zones.\n"
              "This will consume a lot of memory and take a long time.")
    else:
        print(f"Writing files for MGRS grid zones {', '.join(zones)}")

    m8bfile = from_file(fname)
    print(f"Number of records: {m8bfile.records}")
    records = 0
    mgrsd = defaultdict(lambda: defaultdict(set))
    for netid, mcoords in m8bfile:
        if (records % 1E6) == 0:
            print(f"Processed records: {records}")

        records += 1
        mzone = mcoords[:3]
        if mzone not in zones:
            continue

        mgrsd[mzone][netid].add(mcoords)

    print(f"Expected num of records: {m8bfile.records}")
    print(f"Found this many records: {records}")

    ext = '.msgpack'
    for mzone, id2coords in mgrsd.items():
        outfname = mzone + ext
        print(f"Writing to file '{outfname}'")

        # msgpack cannot serialize sets, so convert them to lists
        outd = {}
        for k, v in id2coords.items():
            outd[k] = list(v)

        with open(outfname, 'wb') as outf:
            msgpack.dump(outd, outf)


def valid_zone(zone):
    zone_re = re.compile(r'\d{2}[C-HJ-NP-X]')
    return bool(re.match(zone_re, zone))


def main():
    desc = (
        "Convert an m8b-file to msgpack files, one per MGRS grid zone\n"
        "(https://en.wikipedia.org/wiki/Military_Grid_Reference_System"
        "#Grid_zone_designation)\n\n"
        "Note that the memory usage of this program is proportional to the\n"
        "number of MGRS grid zones given. The program will consume a lot of memory\n"
        "if all MGRS grid zones are specified.\n\n"
    )  # noqa
    parser = argparse.ArgumentParser(description=desc,
                                     formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('filename', help="The m8b-file to convert")
    parser.add_argument('zones', nargs='+', metavar='zone',
                        help="One or more MGRS grid zones to convert")
    args = parser.parse_args()

    invalid_zones = [z for z in args.zones if not valid_zone(z)]
    if invalid_zones:
        print(f"Invalid MGRS grid zones: {' '.join(invalid_zones)}")
    else:
        m8b_to_msgpacks(args.filename, args.zones)


if __name__ == '__main__':
    main()
