#!/usr/bin/env python3
import asyncio
import logging
import sys
from datetime import datetime

from aiohttp import web
import fastjsonschema  # type: ignore

from . import __version__
from . import config
from . import obsdb
from . import schemas
from .locator.clustering import ClusteringLocator
from .locator.m8b import M8BLocator
from .locator.single import StrongestCellLocator, StrongestWifiLocator
from .locator.web import WebLocator
from .resolver.cellid import CellIdResolver
from .resolver.local import get_localresolver
from .resolver.wiglenet import WiglenetCacheOnlyResolver, WiglenetResolver

locator_groups: list[list] = []
localdbs: list[object] = []
geolocate_validate = fastjsonschema.compile(schemas.geolocate_v1_schema)
geosubmit_validate = fastjsonschema.compile(schemas.geosubmit_v2_schema)
hint = None


async def all_handler(request):
    return web.json_response({}, status=404)


async def locate_handler(request):
    global hint
    global locator_groups

    notfound_error_res = {
        'error': {
            'errors': [{
                'domain': 'geolocation',
                'reason': 'notFound',
                'message': 'Not found',
            }],
            'code': 404,
            'message': 'Not found',
        }
    }
    parse_error_res = {
        # 'details': {
        #     'decode': "JSONDecodeError('Expecting value: line 1 column 1 (char 0)')"
        # },
        'error': {
            'code': 400,
            'errors': [
                {
                    'domain': 'global',
                    'message': 'Parse Error',
                    'reason': 'parseError'
                }
            ],
            'message': 'Parse Error'
        }
    }

    unvalidated = await request.json()
    try:
        data = geolocate_validate(unvalidated)
    except fastjsonschema.JsonSchemaValueException as e:
        log.warning('Input JSON did not validate: ' + str(e))
        log.debug(unvalidated)
        return web.json_response(parse_error_res, status=400)

    # Make sure all cell items have a radioType and convert it to upper case
    if 'cellTowers' in data:
        data['radioType'] = data.get('radioType', 'GSM').upper()
    for cell in data.get('cellTowers', []):
        cell['radioType'] = cell.get('radioType', data['radioType']).upper()

    log.info("Locate request from '%s' with %s" % (
        request.headers['user-agent'],
        ', '.join(f'{len(data[k])} {k}' for k in data if k in (
                  'bluetoothBeacons', 'cellTowers', 'wifiAccessPoints', 'fallbacks'))))
    data['time'] = datetime.now().astimezone()
    for station in data.get('bluetoothBeacons', []) + data.get('wifiAccessPoints', []):
        station['macAddress'] = station['macAddress'].lower()

    tasks = []
    results = []
    try:
        async with asyncio.timeout(10):
            for lgroup in locator_groups:
                async with asyncio.TaskGroup() as tg:
                    for locator in lgroup:
                        tasks.append(tg.create_task(
                            locator.locate(data, hint=hint), name=locator.name))

                for task in (t for t in tasks if t.done() and not t.cancelled()):
                    res = task.result()
                    if res is not None and res[0] is not None:
                        results.append((res[0], res[1], task.get_name()))

                if results:
                    break  # Do not query the remaining groups

    except asyncio.TimeoutError:
        log.warning("Locator(s) timed out: %s" % (
            ', '.join(t.get_name() for t in tasks if t.cancelled())))

    if results:
        results.sort(key=lambda x: x[1] if x[1] is not None else 1e6)
        best = results[0]
        latlon, accuracy = best[0], best[1]
        log.info("Locator(s) with results: %s" % (', '.join(r[2] for r in results)))
    else:
        log.info("No results from locators")
        latlon, accuracy = None, None

    if latlon is not None:
        hint = latlon
        status = 200
        resd = {
            'location': {
                'lat': latlon[0],
                'lng': latlon[1],
            },
        }
        if accuracy is not None and accuracy >= 0:
            resd['accuracy'] = accuracy
    else:
        hint = None
        status = 404
        resd = notfound_error_res

    for ldb in localdbs:
        ldb.insert_locate(data, latlon, accuracy)

    return web.json_response(resd, status=status)


async def submit_handler(request):
    global observationdb

    parse_error_res = {
        # 'details': {
        #     'validation': {
        #         'items': 'Required'
        #     },
        # },
        'error': {
            'code': 400,
            'errors': [
                {
                    'domain': 'global',
                    'message': 'Parse Error',
                    'reason': 'parseError'
                }
            ],
            'message': 'Parse Error'
        },
    }

    unvalidated = await request.json()
    try:
        data = geosubmit_validate(unvalidated)
    except fastjsonschema.JsonSchemaValueException as e:
        log.warning('submit: Input JSON did not validate: ' + str(e))
        log.debug(unvalidated)
        return web.json_response(parse_error_res, status=400)

    log.info("Got submission from '%s' with %d items",
             request.headers['user-agent'], len(data['items']))
    for ldb in localdbs:
        ldb.insert_submit(data)

    return web.json_response({}, status=200)


def main():
    global locator_groups
    global log

    try:
        conf = config.get_config()
    except config.ConfigError as e:
        print(str(e))
        sys.exit(1)
    except FileNotFoundError as e:
        print('Could not load config: ' + str(e))
        sys.exit(1)

    timestampformat = '%(asctime)s.%(msecs)03d ' if conf['timestamps'] else ''
    logging.basicConfig(
        level=logging.WARNING,  # Loglevel for library modules
        format=timestampformat + '%(module)s %(levelname)s: %(message)s',
        datefmt='%Y-%m-%d %H:%M:%S')
    log = logging.getLogger(__name__)
    logging.getLogger('ols').setLevel(conf['loglevel'])

    log.info(f"OLS version {__version__} starting up")

    def get_resolver(name):
        rconf = conf['resolver'].get(name)
        if rconf is None:
            log.critical(f"Resolver '{name}' not found in config")
            sys.exit(1)
        rtype = rconf.get('type')
        match rtype:
            case 'cellid':
                return CellIdResolver(rconf['db'], name=name)
            case 'wiglenet':
                return WiglenetResolver(
                    rconf['apiuser'], rconf['apitoken'], rconf['db'], name=name)
            case 'wiglenetcache':
                return WiglenetCacheOnlyResolver(rconf['db'], name=name)
            case 'localdb':
                return get_localresolver(rconf['db'], name=name)
            case _:
                log.critical(f"Resolver '{name}' has unknown type '{rtype}'")
                sys.exit(1)

    locator_groups = []
    for group in conf['methods']:
        locator_group = []
        for locator in group:
            lconf = conf['locator'].get(locator)
            if lconf is None:
                log.critical(f"Locator '{locator}' is in 'methods' "
                             "but not found in locator section")
                sys.exit(1)
            ltype = lconf.get('type')
            match ltype:
                case 'clustering':
                    wlist = lconf.get('wifiresolvers', [])
                    wifiresolvers = [get_resolver(e) for e in wlist]
                    clist = lconf.get('cellresolvers', [])
                    cellresolvers = [get_resolver(e) for e in clist]
                    locator_group.append(
                        ClusteringLocator(wifiresolvers, cellresolvers, name=locator))
                case 'strongestcell':
                    clist = lconf.get('cellresolvers', [])
                    cellresolvers = [get_resolver(e) for e in clist]
                    locator_group.append(
                        StrongestCellLocator(cellresolvers, name=locator))
                case 'strongestwifi':
                    wlist = lconf.get('wifiresolvers', [])
                    wifiresolvers = [get_resolver(e) for e in wlist]
                    locator_group.append(
                        StrongestWifiLocator(wifiresolvers, name=locator))
                case 'm8b':
                    locator_group.append(
                        M8BLocator(lconf['datafile'], name=locator))
                case 'web':
                    locator_group.append(
                        WebLocator(lconf['locateurl'], name=locator))
                case _:
                    log.critical(f'Unknown locator type in config: {ltype}')
                    sys.exit(1)
        locator_groups.append(locator_group)

    for gnum, group in enumerate(locator_groups):
        log.debug(f"Locator group {gnum} is "
                  "%s" % ', '.join(f"'{loc.name}'" for loc in group))

    if conf['obsdb'] is not None:
        localdbs.append(obsdb.ObservationDB(conf['obsdb']))
        log.info(f"Recording all observations to '{conf['obsdb']}'")

    for f in conf['localdb_files']:
        localdbs.append(get_localresolver(f))
        log.info(f"Will update local database '{f}'")

    app = web.Application()
    app.add_routes([
        web.post('/v1/geolocate', locate_handler),
        web.post('/v2/geosubmit', submit_handler),
        web.route('*', '/{path}', all_handler),
        web.route('*', '/', all_handler),
    ])

    log.info('Starting service...\n')
    web.run_app(app, host=conf['host'], port=conf['port'])
    log.info('Stopping service...\n')


if __name__ == '__main__':
    main()
