import asyncio
import itertools
import logging
import re
import sys
from datetime import datetime
from typing import Optional, Union

import aiohttp
import fastjsonschema  # type: ignore
import mercantile  # type: ignore
import numpy as np

from .wiglenetdb import WiglenetDB
from .wiglenetschemas import (
    cell_re,
    wiglenet_v2_search_cell_schema,
    wiglenet_v2_search_wifi_schema,
    wiglenet_v3_detail_valid_schema,
)
from ..base import ResolverBase
from ...constants import (
    CELL_DEFAULT_RADIUS,
    CELL_MIN_RADIUS,
    CELL_TYPES,
    WIFI_DEFAULT_RADIUS,
    WIFI_MIN_RADIUS,
)
from ...schemas import mac_re
from ...utils import (
    circle_radius,
    ichnaea_score,
    min_or_none,
    webmercator_tile,
)

log = logging.getLogger(__name__)


def netd_type(netd):
    # TODO: bluetooth
    ntype = netd.get('type', '')
    if ntype in ('ad-hoc', 'infra'):
        return 'wifi'
    elif ntype in CELL_TYPES:
        return 'cell'
    else:
        netid = netd.get('networkId', '')
        if re.match(mac_re, netid):
            return 'wifi'
        elif re.match(cell_re, netid):
            return 'cell'
        else:
            return 'unknown'


def radius_from_wiglenet(netd) -> float:
    ntype = netd_type(netd)
    if ntype == 'wifi':
        default_radius = WIFI_DEFAULT_RADIUS
        min_radius = WIFI_MIN_RADIUS
    elif ntype == 'cell':
        default_radius = CELL_DEFAULT_RADIUS
        min_radius = CELL_MIN_RADIUS
    else:
        return WIFI_DEFAULT_RADIUS

    clusters = netd.get('locationClusters')
    if clusters is None:
        return default_radius

    cluster = clusters[np.argmax([c['score'] for c in clusters])]
    locations = cluster['locations']
    if not locations:
        return default_radius

    max_lat = max(p['latitude'] for p in locations)
    min_lat = min(p['latitude'] for p in locations)
    max_lon = max(p['longitude'] for p in locations)
    min_lon = min(p['longitude'] for p in locations)
    center = netd['trilateratedLatitude'], netd['trilateratedLongitude']
    radius = circle_radius(center, (max_lat, min_lon), (min_lat, max_lon))
    return radius if radius >= min_radius else default_radius


def modelize_wiglenet(netd) -> Optional[dict]:
    """Make an observation model from a wigle.net API result.

    The model is a dict with keys 'latlon', 'radius', 'age', 'score'.
    """
    if netd is None:
        return None

    radius = netd.get('_radius') or radius_from_wiglenet(netd)
    if radius < 0:
        radius = WIFI_DEFAULT_RADIUS

    clusters = netd.get('locationClusters')
    if clusters:
        cluster = clusters[np.argmax([c['score'] for c in clusters])]
        locations = cluster.get('locations') or []
        firstseen, lastseen = cluster['minLastUpdate'], cluster['maxLastUpdate']
        # Consider multiple clustered observations as one
        samples = len(locations) if radius > WIFI_MIN_RADIUS else 1
    else:
        firstseen, lastseen = netd['firstSeen'], netd['lastSeen']
        samples = netd['bestClusterWiGLEQoS']  # better than nothing

    now = int(datetime.now().timestamp() * 1e6)
    return {
        'latlon': (netd['trilateratedLatitude'], netd['trilateratedLongitude']),
        'radius': radius,
        'age': now - netd['lastSeen'],  # usec
        'score': ichnaea_score(now, firstseen, lastseen, samples)
    }


def timestr_to_usec(tstr) -> int:
    """Parse an ISO 8601 time string, return usecs from epoch as int, or 0."""
    try:
        return int(  # ISO 8601 parsing in datetime is borken
            datetime.fromisoformat(tstr.replace('Z', '')).timestamp() * 1e6)
    except ValueError:
        return 0


def convert_timestrs(wigled):
    """Convert time strings in v3 detail API response dicts _in place_.

    Converts ISO time strings to usec from epoch integers.
    """
    def dconvert(dd, timekeys):
        for k in timekeys:
            dd[k] = timestr_to_usec(dd.get(k))

    dconvert(wigled, ('firstSeen', 'lastSeen', 'lastUpdate'))
    for cd in wigled.get('locationClusters', []):
        dconvert(cd, ('maxLastUpdate', 'minLastUpdate'))
        for ld in cd.get('locations', []):
            dconvert(ld, ('lastupdt', 'time'))


class TooManyQueriesException(Exception):
    pass


class BadResponseError(Exception):
    pass


class WiglenetResolver(ResolverBase):
    """Resolve netids to observation models with wigle.net.

    Caches the API responses to an sqlite database.
    """

    def __init__(self, apiuser, apitoken, dbfile, name="Unknown WiglenetResolver"):
        self.name = name
        self.apibase = 'https://api.wigle.net/api/'
        self.auth = aiohttp.BasicAuth(apiuser, apitoken)
        self.db = WiglenetDB(dbfile)
        # If we made too many detail or search queries today (429), put timestamps here
        self.detailq_disabled_at = None
        self.searchq_disabled_at = None
        self.pause = 2 * 3600  # Waittime (s) before allowing queries again
        self.tile_min_z = 19  # Default zoom for tile areas used for v2 search queries
        self.tile_max_z = 21  # Stop recursive tile update on this zoom level
        self.tile_expiry = 180 * 24 * 3600  # Refetch search tiles after this time (s)
        self.search_wifi_validate = fastjsonschema.compile(
            wiglenet_v2_search_wifi_schema)
        self.search_cell_validate = fastjsonschema.compile(
            wiglenet_v2_search_cell_schema)
        self.detail_validate = fastjsonschema.compile(wiglenet_v3_detail_valid_schema)
        self.search_lock = asyncio.Lock()
        self.detail_lock = asyncio.Lock()
        self._background_tasks = set()

    def can_search(self):
        if (self.searchq_disabled_at is not None
                and (datetime.now().timestamp() - self.searchq_disabled_at)
                < self.pause):
            return False
        else:
            self.searchq_disabled_at = None
            return True

    async def _v2_search_apicall(self, nettype, latrange, lonrange) -> Optional[dict]:
        if not self.can_search():
            log.debug('search: queries are disabled')
            raise TooManyQueriesException

        apinet = {
            'wifi': 'network',
            'cell': 'cell',
            # 'bt': 'bluetooth'
        }[nettype]
        url = self.apibase + f'v2/{apinet}/search'
        params = {
            'latrange1': latrange[0],
            'latrange2': latrange[1],
            'longrange1': lonrange[0],
            'longrange2': lonrange[1],
            # resultsPerPage = 0 results in:
            # 400 Client Error: Bad Request for url:
            'resultsPerPage': 1000,
        }

        total_results = -2
        resultcount = 0
        finald = None
        while True:
            async with aiohttp.ClientSession(auth=self.auth) as session:
                log.debug(f'search: API call with {url=}, {params=}')
                async with session.get(url, params=params) as response:
                    # log.debug(f'search: Got {response.status=}')
                    if (response.status == 200
                            and response.headers['content-type'] == 'application/json'):
                        pass
                    elif response.status == 400:
                        log.error('search: Request body error')
                        break
                    elif response.status == 410:
                        log.error('search: Query failed')
                        break
                    elif response.status == 429:
                        log.debug(f'search: Disabling queries for {self.pause} seconds')
                        self.searchq_disabled_at = datetime.now().timestamp()
                        raise TooManyQueriesException
                    else:
                        log.error(
                            f'search: Bad response: {response.status}, content-type: '
                            + response.headers['content-type'])
                        log.debug('search: text: ' + await response.text())
                        raise BadResponseError('search: Did not get JSON')
                        break

                    unvalidated = await response.json()
                    try:
                        if nettype == 'wifi':
                            data = self.search_wifi_validate(unvalidated)
                        elif nettype == 'cell':
                            data = self.search_cell_validate(unvalidated)
                            # Fix the cell dict to be compatible with the wifi dict
                            for net in data['results']:
                                net['netid'] = net['id']
                                net['type'] = net['gentype']
                    except fastjsonschema.JsonSchemaValueException as e:
                        log.warning('search: JSON did not validate: ' + str(e))
                        log.debug(unvalidated)
                        break

                    if finald is None:
                        finald = data
                        total_results = data.get('totalResults', -1)
                        log.debug(f'search: {total_results=}')
                    else:  # Merge
                        finald['results'].extend(data.get('results'))

                    if data.get('success', False) is False:
                        log.error('search: unsuccessful')

                    reslen = len(data.get('results', []))
                    resultcount += reslen

                    search_after = data.get('searchAfter')
                    params['searchAfter'] = search_after

                    if (search_after is None or resultcount >= total_results):
                        log.debug(f'search: Done, {resultcount=}')
                        break

        if finald is None:
            return None

        n_final_results = len(finald.get('results', []))
        if resultcount != n_final_results:
            log.warning(
                f'search: Retrieved {resultcount} results, retained {n_final_results}')
        finald['last'] = n_final_results
        finald.pop('searchAfter')
        finald.pop('search_after')
        now = int(datetime.now().timestamp() * 1e6)
        finald['_retrieved'] = now

        # Convert timestamps and macs in place
        for resd in finald.get('results', []):
            resd['firsttime'] = timestr_to_usec(resd['firsttime'])
            resd['lasttime'] = timestr_to_usec(resd['lasttime'])
            resd['lastupdt'] = timestr_to_usec(resd['lastupdt'])
            resd['netid'] = resd['netid'].lower()

        for resd in finald.get('results', []):
            self.db.insert_network({
                'bestClusterWiGLEQoS': resd['qos'],
                'firstSeen': resd['firsttime'],
                'lastSeen': resd['lasttime'],
                'lastUpdate': resd['lastupdt'],
                'networkId': resd['netid'],
                'trilateratedLatitude': resd['trilat'],
                'trilateratedLongitude': resd['trilong'],
                'type': resd['type'],
                '_retrieved': now,
                '_radius': -1.0,  # Mark results from search
            })

        return finald

    async def _tileupdate(self, nettype, tile) -> bool:
        """Update search tile in the cache, if needed.

        Makes a search API call if the tile is not cached, or it has expired,
        that is, the tile has not been updated in self.tile_timout seconds.

        Returns True if the cache was updated and the cache potentially
        contains new values, False otherwise.
        """
        now = int(datetime.now().timestamp() * 1e6)

        async def recursive_update(
                tile, networks_updated) -> tuple[Optional[int], bool]:
            """Update the networks belonging to a tile.

            Returns a tuple (retrieved, networks_updated). The first member
            retrieved is a timestamp of the last data update, or None,
            if the update failed. The second member networks_updated is a
            boolean indicating whether any search API call was successfully
            made.

            If a tile exists in the DB and download has been attempted before
            (retrieved == None), split the tile and try to update its children.
            This is made recursively, until tile.z > self.tile_max_z.

            If a tile does not exist in DB or is expired, try to update it
            as one piece with the v2 search API. If this fails for any reason,
            return (None, networks_updated).

            If a tile exists in the database and is up to date, return
            (<existing retrieved timestamp>, False).
            """
            if not self.can_search():
                log.debug(f'tileupdate: queries are disabled ({nettype} {tile=})')
                return None, networks_updated
            dbtile = self.db.find_searchtile(nettype, tile)
            if (dbtile is not None
                    and dbtile['retrieved'] is None
                    and dbtile['z'] < self.tile_max_z):
                # Tile was only partially fetched, recurse
                log.debug(f'tileupdate: Updating children of {nettype} {tile=}')
                children = mercantile.children(tile)
                child_retrieved: list[Optional[int]] = []
                any_updated = False
                all_updated = True
                nets_updated = networks_updated
                for ctile in children:
                    retrieved, n_updated = await recursive_update(
                        ctile, networks_updated)
                    updated = retrieved is not None
                    child_retrieved.append(retrieved)
                    any_updated |= updated
                    all_updated &= updated
                    nets_updated |= n_updated

                if all_updated:
                    log.debug(f'tileupdate: {nettype} {tile=} fully updated')
                    self.db.delete_searchtile_children(nettype, tile)
                    return min_or_none(child_retrieved), nets_updated
                elif any_updated:
                    for i, ctile in enumerate(children):
                        log.debug(f'tileupdate: Adding {nettype} {ctile=} to cache')
                        self.db.insert_or_replace_searchtile(
                            nettype, ctile, child_retrieved[i])
                    return None, nets_updated
                else:
                    return None, nets_updated
            elif (dbtile is None
                    or (dbtile['retrieved'] is None and dbtile['z'] >= self.tile_max_z)
                    or (now - dbtile['retrieved']) > self.tile_expiry * 1e6):
                # Tile did not exist, is expired or max_z reached
                log.debug(f'tileupdate: Downloading {nettype} {tile=}')
                bb = mercantile.bounds(tile)
                try:
                    # log.debug(f'tileupdate: Making {nettype} v2 search API call')
                    res = await self._v2_search_apicall(
                        nettype, (bb.south, bb.north), (bb.west, bb.east))
                except (TooManyQueriesException, BadResponseError):
                    return None, networks_updated
                retrieved = (None if res is None
                             else (now if res['totalResults'] == len(res['results'])
                                   else None))
                return retrieved, True
            else:
                # Tile is up to date
                # log.debug(f'tileupdate: {nettype} {tile=} is current')
                return dbtile['retrieved'], networks_updated

        retrieved, networks_updated = await recursive_update(tile, False)
        if networks_updated:
            self.db.insert_or_replace_searchtile(nettype, tile, retrieved)

        return networks_updated

    async def _update_neighbour_tiles(self, nettype, center) -> bool:
        """Update tiles surrounding the given center tile.

        Return True if updates to tile cache have been made.
        """
        if not self.can_search():
            log.debug('update_neighbour_tiles: queries are disabled')
            return False

        networks_updated = False
        neighbours = (x for x in itertools.product(
                      range(-1, 2), range(-1, 2)) if x != (0, 0))
        try:
            for x, y in neighbours:
                tile = mercantile.Tile(center.x + x, center.y + y, center.z)
                async with asyncio.timeout(30):
                    async with self.search_lock:
                        networks_updated |= await self._tileupdate(nettype, tile)
        except TimeoutError:
            log.debug("Timeout on _update_neighbour_tiles")

        return networks_updated

    async def _v3_detail_apicall(self, nettype, key) -> Optional[dict]:
        if (self.detailq_disabled_at is not None
                and (datetime.now().timestamp() - self.detailq_disabled_at)
                < self.pause):
            log.debug('detail: queries are disabled')
            raise TooManyQueriesException
        else:
            self.detailq_disabled_at = None

        assert nettype in ('wifi', 'cell')
        if nettype == 'wifi':
            url = self.apibase + f'v3/detail/{nettype}/{key}'
            networkid = key
        elif nettype == 'cell':
            radiotype, opc, lac, cid = key
            url = self.apibase + f'v3/detail/cell/{radiotype}/{opc}/{lac}/{cid}'
            networkid = f'{opc}_{lac}_{cid}'

        log.debug(f'detail: Querying at {url}')
        async with aiohttp.ClientSession(auth=self.auth) as session:
            async with session.get(url) as response:
                log.debug(f'detail: Got {response.status=}')
                if (response.status == 200
                        and response.headers['content-type'] == 'application/json'):
                    log.debug(f'detail: Got direct result for {url=}')
                    unvalidated = await response.json()
                    try:
                        netd = self.detail_validate(unvalidated)
                    except fastjsonschema.JsonSchemaValueException as e:
                        log.warning('detail: JSON did not validate: ' + str(e))
                        log.debug(unvalidated)
                        return None
                    convert_timestrs(netd)
                    netd['networkId'] = netd['networkId'].lower()
                    netd['_radius'] = radius_from_wiglenet(netd)
                    self.db.insert_network(netd)
                    return netd
                elif (response.status == 404):
                    log.debug(f'detail: Network not found for {url=}')
                    netd = {k: None for k in (
                        'bestClusterWiGLEQoS',
                        'firstSeen',
                        'lastSeen',
                        'lastUpdate',
                        'trilateratedLatitude',
                        'trilateratedLongitude',
                        'type',
                        '_radius')}
                    netd['networkId'] = networkid
                    self.db.insert_network(netd)
                    return None
                elif (response.status == 429):
                    self.detailq_disabled_at = datetime.now().timestamp()
                    log.debug(f'detail: disabling queries for {self.pause} seconds')
                    raise TooManyQueriesException
                else:
                    log.error(
                        f'detail: Bad response: {response.status}, content-type: '
                        + response.headers['content-type'])
                    log.debug('detail: text: ' + await response.text())
                    raise BadResponseError('detail: Did not get JSON')

    def _cachecall(self, netid) -> Union[dict, None, bool]:
        """Possibly return a cached value for given netid.

        Returns:
        dict netd if netid is in DB as has an entry with coordinates,
        None if netid is in DB, but is a cached 'Not Found' wigle.net reply,
        False if netid is not in DB.
        """
        netd = self.db.search_network(netid)
        if netd is None:
            return False
        else:
            restype = ("'Not found'"
                       if netd.get('trilateratedLatitude') is None else 'Location')
            log.debug(f'{restype} cache hit, {netid}')
            if netd.get('trilateratedLatitude') is None:
                return None

        return netd

    async def _get_net(
            self, nettype, key, hint=None, cacheonly=False) -> Optional[dict]:
        if nettype == 'wifi':
            netid = key
        elif nettype == 'cell':
            radiotype, opc, lac, cid = key
            netid = f'{opc}_{lac}_{cid}'

        cacheres = self._cachecall(netid)
        if not isinstance(cacheres, bool):
            return cacheres

        if cacheonly:
            return None

        # Make v3 detail API call first, then make v2 search
        # on its tile to populate cache
        # FIXME: Make a function like g_network_monitor_can_reach () and use it

        if self.detail_lock.locked():
            # If lock was held, it's likely that another task was doing the same
            # query, so check the cache again after getting the lock.
            await self.detail_lock.acquire()
            cacheres = self._cachecall(netid)
            if not isinstance(cacheres, bool):
                self.detail_lock.release()
                return cacheres
        else:
            await self.detail_lock.acquire()
        try:
            # log.debug(f'Making v3 detail API call for {nettype} {key}')
            netd = await self._v3_detail_apicall(nettype, key)
            searchcoords = (
                hint if netd is None
                else (netd['trilateratedLatitude'], netd['trilateratedLongitude']))
        except (TooManyQueriesException, BadResponseError):
            netd = None
            # log.debug(f'Using {hint=}')
            searchcoords = hint
        except aiohttp.ClientError as e:
            log.error('Client error on detail query: ' + str(e))
            netd = None
            # log.debug(f'Using {hint=}')
            searchcoords = hint
        finally:
            self.detail_lock.release()

        if searchcoords is not None and self.can_search():
            tile = webmercator_tile(searchcoords[0], searchcoords[1], self.tile_min_z)
            try:
                # timeout needs to be lower than 20 s locate query timeout in server.py
                async with asyncio.timeout(15):
                    async with self.search_lock:
                        updated = await self._tileupdate(nettype, tile)
            except TimeoutError:
                log.debug("Timeout on _tileupdate")
                updated = False
            except aiohttp.ClientError as e:
                log.error('Client error on search query: ' + str(e))
                updated = False
            if updated and netd is None:
                cacheres = self._cachecall(netid)
                if not isinstance(cacheres, bool):
                    return cacheres
            task = asyncio.create_task(self._update_neighbour_tiles(nettype, tile))
            self._background_tasks.add(task)
            task.add_done_callback(self._background_tasks.discard)

        return netd

    async def resolve_wifi(
            self, mac, hint=None, cacheonly=False, **kwargs) -> Optional[dict]:
        netd = await self._get_net('wifi', mac, hint=hint, cacheonly=cacheonly)
#        log.debug('Resolving {} for wifi {}'.format(
#            'failed' if netd is None else 'successful', mac))
        return modelize_wiglenet(netd)

    async def resolve_cell(
            self, tec, opc, lac, cid, hint=None, cacheonly=False,
            **kwargs) -> Optional[dict]:
        # FIXME: An explicit MLS to wigle tec conversion would be better
        rtype = tec.upper()
        netd = await self._get_net(
            'cell', (rtype, opc, lac, cid), hint=hint, cacheonly=cacheonly)
#        log.debug('Resolving {} for cell {}'.format(
#            'failed' if netd is None else 'successful', f'{opc}_{lac}_{cid}'))
        return modelize_wiglenet(netd)


class WiglenetCacheOnlyResolver(object):
    """Resolve netids to observation models with the local wigle.net cache."""

    def __init__(self, dbfile, name="Unknown WiglenetCacheOnlyResolver"):
        self.name = name
        self.db = WiglenetDB(dbfile)

    async def _get_net(self, nettype, key, hint=None) -> Optional[dict]:
        if nettype == 'wifi':
            netid = key
        elif nettype == 'cell':
            radiotype, opc, lac, cid = key
            netid = f'{opc}_{lac}_{cid}'

        netd = self.db.search_network(netid)
        if netd is None:
            return None
        else:
            restype = ("'Network not found'"
                       if netd.get('trilateratedLatitude') is None else 'location')
            log.debug(f'Cache-only {restype} result for {netid}')
            if netd.get('trilateratedLatitude') is None:
                return None
        return netd

    async def resolve_wifi(self, mac, **kwargs) -> Optional[dict]:
        netd = await self._get_net('wifi', mac)
        return modelize_wiglenet(netd)

    async def resolve_cell(self, radiotype, opc, lac, cid, **kwargs) -> Optional[dict]:
        # FIXME: An explicit MLS to wigle radiotype conversion would be better
        rtype = radiotype.upper()
        netd = await self._get_net(
            'cell', (rtype, opc, lac, cid))
        return modelize_wiglenet(netd)


def main():
    global log
    import os.path
    import re
    try:
        import tomllib  # type: ignore
    except (ImportError, ModuleNotFoundError):
        import tomli as tomllib

    configfilename = sys.argv[1]
    net = sys.argv[2]

    with open(configfilename, 'r') as c:
        conf = tomllib.loads(c.read())

    logging.basicConfig(
        level=logging.WARNING,  # Loglevel for library modules
        format='[%(asctime)s.%(msecs)03d] %(module)s %(levelname)s: %(message)s',
        datefmt='%Y-%m-%d %H:%M:%S')
    log = logging.getLogger(__name__)
    logging.getLogger('ols').setLevel(logging.DEBUG)

    wconf = next(b for b in conf.get('resolver', {}).values()
                 if b.get('type', '') == 'wiglenet')
    dbfile = os.path.join(conf['datadir'], wconf['db'])
    resolver = WiglenetResolver(wconf['apiuser'], wconf['apitoken'], dbfile)
    if re.match(r'^(?:[0-9a-fA-F]{2}:){5}[0-9a-fA-F]{2}$', net):
        result = asyncio.run(resolver.resolve_wifi(net))
    elif re.match(r'\d+_\d+_\d+', net):
        opc, lac, cid = net.split('_')
        radiotype = 'LTE'
        result = asyncio.run(resolver.resolve_cell(radiotype, opc, lac, cid))

    print(result)
