import logging
import sqlite3
from datetime import datetime
from typing import Optional

import mercantile  # type: ignore


VERSION = 1
log = logging.getLogger(__name__)


class WiglenetDB(object):

    def __init__(self, filename) -> None:
        self.con = sqlite3.connect(filename)
        self.con.row_factory = sqlite3.Row
        self.maybe_create()
        log.debug('Database version: ' + str(self.get_version()))

    def get_version(self) -> int:
        cur = self.con.cursor()
        return cur.execute('PRAGMA user_version').fetchone()[0]

    def maybe_create(self) -> None:
        """DB with minimal representation of wigle.net data.

        Variables unnecessary to location are not included, selected wifi and
        cell keys are combined to a single table.
        """
        version = self.get_version()
        if version != 0 and version < VERSION:
            log.error('Old version of the database detected, exiting')
            raise ValueError

        cur = self.con.cursor()
        cur.execute("""
            CREATE TABLE IF NOT EXISTS network (
                -- # Sqlite creates a rowid automatically: rowid INTEGER
                bestClusterWiGLEQoS INTEGER,
                firstSeen INTEGER, -- usec from epoch
                lastSeen INTEGER, -- usec from epoch
                lastUpdate INTEGER, -- usec from epoch
                networkId TEXT, -- mac for wifi/bt, opc_lac_cid tuple for cell
                trilateratedLatitude REAL,
                trilateratedLongitude REAL,
                type TEXT, -- 'infra' for wifi, GSM, WCDMA or LTE for cell
                -- # OLS additions:
                -- # Timestamp of last update from wigle.net
                _retrieved INTEGER, -- usec from epoch
                -- # Range of the station used for location algorithms
                _radius REAL, -- meters
                -- # Not included
                -- attributes (list of strings, not included, only in cell)
                -- bcninterval INTEGER, -- Only in wifi
                -- channel INTEGER, -- Not needed
                -- comment TEXT,    -- Only in wifi
                -- dhcp TEXT,       -- Only in wifi
                -- encryption TEXT, -- Only in wifi
                -- freenet TEXT,    -- Only in wifi
                -- name TEXT,       -- Only in wifi
                -- paynet TEXT,     -- Only in wifi
                -- streetAddress    -- Dict, not included
                -- xarfcs  -- unknown function, only in cell
                PRIMARY KEY (networkId)
            );
        """)
        cur.execute("""
            CREATE INDEX IF NOT EXISTS idx_network_id ON network(networkId);
        """)
        cur.execute("""
            CREATE TABLE IF NOT EXISTS locationCluster (
                -- # Sqlite creates a rowid automatically: rowid INTEGER
                _network_rowid INTEGER, -- parent
                maxLastUpdate INTEGER,
                minLastUpdate INTEGER,
                score INTEGER
                -- centroidLatitude REAL,
                -- centroidLongitude REAL,
                -- clusterSsid TEXT,
                -- daysObservedCount INTEGER,
            );
        """)
        cur.execute("""
            CREATE INDEX IF NOT EXISTS idx_cluster_net_id ON locationCluster(
                _network_rowid);
        """)
        cur.execute("""
            CREATE TABLE IF NOT EXISTS location (
                _cluster_rowid INTEGER, -- parent
                genType TEXT, -- Only in cell: GSM, WCDMA or LTE
                lastupdt INTEGER, -- usec from epoch
                latitude REAL,
                longitude REAL,
                time INTEGER -- Observation time, usec from epoch
                -- accuracy INTEGER
                -- alt INTEGER,
                -- attributes TEXT, -- Only in cell
                -- channel INTEGER,
                -- encryptionValue TEXT, -- Only in wifi
                -- month TEXT, -- Redundant
                -- name TEXT, -- Only in wifi
                -- netId TEXT, -- wifi: mac hash?, cell: opc_lac_cid tuple
                -- noise INTEGER, -- Only in wifi
                -- signal INTEGER,
                -- snr INTEGER, -- Only in wifi
                -- ssid TEXT, -- Operator name for cell
                -- wep TEXT -- Only in wifi
            );
        """)
        cur.execute("""
            CREATE INDEX IF NOT EXISTS idx_loc_cluster_id ON location(_cluster_rowid);
        """)
        cur.execute("""
            CREATE TABLE IF NOT EXISTS searchtile (
                -- Web Mercator coordinates of tile download area
                nettype TEXT, -- 'w' for wifi, 'c' for cell, 'b' for bluetooth
                x INTEGER,
                y INTEGER,
                z INTEGER,
                retrieved INTEGER, -- usec, if NULL, then retrieval was not complete
                PRIMARY KEY (nettype, x, y, z)
            );
        """)
        cur.execute(f'PRAGMA user_version = {VERSION}')
        self.con.commit()

    def search_network(self, network_id) -> Optional[dict]:
        """Return a cached wigle.net API response from the DB.

        None is returned if the given network_id is not in the DB.
        'Not Found' results from wigle.net return a dict with None as values
        for all keys, except 'networkId' and '_retrieved'.
        """
        cur = self.con.cursor()
        net = cur.execute("""
            SELECT rowid, * FROM network WHERE networkId = ?;
            """, (network_id,)).fetchone()

        if net is None:
            return None

        netd = {k: net[k] for k in net.keys() if k != 'rowid'}
        clusters = cur.execute("""
            SELECT rowid, * FROM locationCluster WHERE _network_rowid = ?;
            """, (net['rowid'],)).fetchall()
        clist = []
        for c in clusters:
            cd = {k: c[k] for k in c.keys() if k not in ('rowid', '_network_rowid')}
            locs = cur.execute("""
                SELECT * FROM location WHERE _cluster_rowid = ?;
                """, (c['rowid'],)).fetchall()
            llist = []
            for loc in locs:
                llist.append({k: loc[k] for k in loc.keys() if k != '_cluster_rowid'})
            cd['locations'] = llist
            clist.append(cd)

        netd['locationClusters'] = clist
        return netd

    def insert_network(self, data) -> None:
        """Insert network data dict to combined network tables.

        If the 'lastUpdate' column is larger in new data than in a possible
        old value, the old value is deleted first before inserting data.
        """
        network_id = data.get('networkId')
        if network_id is None:
            log.error("No 'networkId' in new data, aborting insert")
            return

        # Allow storing failed queries with most keys None
        new_last_update = data.get('lastUpdate') or 0

        cur = self.con.cursor()
        row = cur.execute("""
            SELECT lastUpdate FROM network WHERE networkId = ?;
            """, (network_id,)).fetchone()
        if row is not None:
            old_last_update = row[0] or 0
            if old_last_update < new_last_update:
                log.debug(f'Deleting old version of {network_id=}')
                self.delete_network(network_id)
            else:
                log.debug('Not inserting older network')
                return

        if data.get('_retrieved') is None:
            data['_retrieved'] = int(datetime.now().timestamp() * 1e6)

        default_gentype = data['type']
        cur.execute("""
            INSERT INTO network VALUES(
                :bestClusterWiGLEQoS,
                :firstSeen, -- usec from epoch
                :lastSeen,  -- usec from epoch
                :lastUpdate,-- usec from epoch
                :networkId,
                :trilateratedLatitude,
                :trilateratedLongitude,
                :type,
                :_retrieved,
                :_radius
            )
        """, data)
        _network_rowid = cur.lastrowid

        for cluster in data.get('locationClusters', []):
            cc = cluster.copy()
            cc['_network_rowid'] = _network_rowid
            locations = cc['locations']
            cur.execute("""
                INSERT INTO locationCluster VALUES(
                    :_network_rowid,
                    :maxLastUpdate,
                    :minLastUpdate,
                    :score
                )
            """, cc)
            _cluster_rowid = cur.lastrowid
            for location in locations:
                loc = location.copy()
                if 'genType' not in loc:
                    loc['genType'] = default_gentype
                loc['_cluster_rowid'] = _cluster_rowid
                cur.execute("""
                    INSERT INTO location VALUES(
                        :_cluster_rowid,
                        :genType,  -- wifi: infra, cell: GSM, WCDMA or LTE
                        :lastupdt, -- usec from epoch
                        :latitude,
                        :longitude,
                        :time -- usec from epoch
                    )
                """, loc)

        self.con.commit()

    def get_network_list(self) -> list[str]:
        """Return a list of all networkIds in the database."""
        cur = self.con.cursor()
        netids = cur.execute("""
            SELECT networkId FROM network;
            """)
        return [n[0] for n in netids.fetchall()]

    def delete_network(self, network_id) -> None:
        cur = self.con.cursor()
        row = cur.execute("""
            SELECT rowid FROM network WHERE networkId = ?;
            """, (network_id,)).fetchone()
        net_rowid = row[0] if row is not None else None
        if net_rowid is None:
            return

        cluster_rowid_rows = cur.execute("""
            SELECT rowid FROM locationCluster WHERE _network_rowid = ?;
            """, (net_rowid,)).fetchall()
        for rowid_row in cluster_rowid_rows:
            cluster_rowid = rowid_row[0]
            cur.execute("""
                DELETE FROM location WHERE _cluster_rowid = ?;
                """, (cluster_rowid,))
            cur.execute("""
                DELETE FROM locationCluster WHERE rowid = ?;
                """, (cluster_rowid,))

        cur.execute("""
            DELETE FROM network WHERE rowid = ?;
            """, (net_rowid,))

        self.con.commit()

    def insert_or_replace_searchtile(self, nettype, tile, retrieved) -> None:
        nt = nettype[0]
        assert nt in ('w', 'c', 'b')
        cur = self.con.cursor()
        cur.execute("""
            INSERT OR REPLACE INTO searchtile VALUES(?, ?, ?, ?, ?);
            """, (nt, tile.x, tile.y, tile.z, retrieved))
        self.con.commit()

    def delete_searchtile(self, nettype, tile) -> None:
        nt = nettype[0]
        cur = self.con.cursor()
        cur.execute("""
            DELETE FROM searchtile
            WHERE nettype = ? AND x = ? AND y = ? AND z = ?;
            """, (nt, tile.x, tile.y, tile.z))
        self.con.commit()

    def delete_searchtile_children(self, nettype, parent_tile) -> None:
        nt = nettype[0]
        tiles = mercantile.children(parent_tile)
        cur = self.con.cursor()
        for tile in tiles:
            cur.execute("""
                DELETE FROM searchtile
                WHERE nettype = ? AND x = ? AND y = ? AND z = ?;
                """, (nt, tile.x, tile.y, tile.z))
        self.con.commit()

    def find_searchtile(self, nettype, tile) -> Optional[dict]:
        nt = nettype[0]
        cur = self.con.cursor()
        row = cur.execute("""
            SELECT * FROM searchtile
            WHERE nettype = ? AND x = ? AND y = ? AND z = ?;
            """, (nt, tile.x, tile.y, tile.z)).fetchone()
        return None if row is None else {k: row[k] for k in row.keys()}
