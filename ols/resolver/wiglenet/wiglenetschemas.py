from ...schemas import mac_re

cell_re = r'^\d+_\d+_\d+$'
mac_or_cell_re = cell_re + '|' + mac_re
time_re = r'^\d{4}-\d\d-\d\dT\d\d:\d\d:\d\d(?:\.\d{1,3}){0,1}Z?$'
# type_enum = [None, '????', 'ad-hoc', 'infra', 'GSM', 'LTE']

wiglenet_v2_search_base_schema = {
    '$schema': 'https://json-schema.org/draft/2020-12/schema',
    '$id': 'wiglenet_v2_search_base',
    'title': 'wigle.net v2 search base response',
    'type': 'object',
    'required': ['searchAfter', 'success', 'results', 'totalResults'],
    'properties': {
        'first': {
            'type': 'integer',
            'description': 'Index of first result in this JSON reply (always 1?)',
            'minimum': 0,
        },
        'last': {
            'type': 'integer',
            'description': 'Index of last result in this JSON reply',
            'minimum': 0,
        },
        'resultCount': {
            'type': 'integer',
            'description': 'Number of results in this JSON reply',
            'minimum': 0,
        },
        'search_after': {
            'type': ['null', 'integer'],
            'description': 'Integer token for getting the next JSON reply',
            'minimum': 0,
        },
        'searchAfter': {
            'type': ['null', 'string'],
            'description': 'Token for getting the next JSON reply, as string',
        },
        'success': {'type': 'boolean'},
        'totalResults': {
            'type': 'integer',
            'description': 'Total number of results for query',
            'minimum': 0,
        },
    }
}

wiglenet_v2_search_wifi_schema = wiglenet_v2_search_base_schema.copy()
wiglenet_v2_search_wifi_schema.update({
    '$id': 'wiglenet_v2_search_wifi',
    'title': 'wigle.net v2 search wifi response',
    'results': {
        'type': 'array',
        'items': {
            'type': 'object',
            'required': [
                'firsttime',
                'gentype',
                'id',
                'lasttime',
                'lastupdt',
                'qos',
                'trilat',
                'trilong',
            ],
            'properties': {
                'bcninterval': {'type': 'integer'},
                'channel': {'type': 'integer'},
                'city': {'type': ['null', 'string']},
                'comment': {'type': ['null', 'string']},
                'country': {'type': ['null', 'string']},
                'dhcp': {'type': ['null', 'string']},
                'encryption': {'type': ['null', 'string']},
                'firsttime': {'type': 'string', 'pattern': time_re},
                'freenet': {'type': ['null', 'string']},
                'housenumber': {'type': ['null', 'string']},
                'lasttime': {'type': 'string', 'pattern': time_re},
                'lastupdt': {'type': 'string', 'pattern': time_re},
                'name': {'type': ['null', 'string']},
                'netid': {'type': 'string', 'pattern': mac_or_cell_re},
                'paynet': {'type': ['null', 'string']},
                'postalcode': {'type': ['null', 'string']},
                'qos': {'type': 'integer'},
                'region': {'type': ['null', 'string']},
                'road': {'type': ['null', 'string']},
                'ssid': {'type': ['null', 'string']},
                'transid': {'type': ['null', 'string']},
                'trilat': {'type': 'number', 'minimum': -90.0, 'maximum': 90.0},
                'trilong': {'type': 'number', 'minimum': -180.0, 'maximum': 180.0},
                # 'type': {'enum': type_enum},
                'type': {'type': ['null', 'string']},
                'userfound': {'type': 'boolean'},
                'wep': {'type': ['null', 'string']},
            },
        }
    }
})

wiglenet_v2_search_cell_schema = wiglenet_v2_search_base_schema.copy()
wiglenet_v2_search_cell_schema.update({
    '$id': 'wiglenet_v2_search_cell',
    'title': 'wigle.net v2 search cell response',
    'results': {
        'type': 'array',
        'items': {
            'type': 'object',
            'required': [
                'firsttime',
                'lasttime',
                'lastupdt',
                'id',
                'qos',
                'trilat',
                'trilong',
                'type',
            ],
            'properties': {
                'channel': {'type': 'integer'},
                'city': {'type': ['null', 'string']},
                'comment': {'type': ['null', 'string']},
                'country': {'type': ['null', 'string']},
                'firsttime': {'type': 'string', 'pattern': time_re},
                'gentype': {'type': ['null', 'string']},
                'housenumber': {'type': ['null', 'string']},
                'lasttime': {'type': 'string', 'pattern': time_re},
                'lastupdt': {'type': 'string', 'pattern': time_re},
                'name': {'type': ['null', 'string']},
                'id': {'type': 'string', 'pattern': cell_re},
                'postalcode': {'type': ['null', 'string']},
                'qos': {'type': 'integer'},
                'region': {'type': ['null', 'string']},
                'road': {'type': ['null', 'string']},
                'ssid': {'type': ['null', 'string']},
                'transid': {'type': ['null', 'string']},
                'trilat': {'type': 'number', 'minimum': -90.0, 'maximum': 90.0},
                'trilong': {'type': 'number', 'minimum': -180.0, 'maximum': 180.0},
                'userfound': {'type': 'boolean'},
            },
        }
    }
})

wiglenet_v3_detail_valid_schema = {
    '$schema': 'https://json-schema.org/draft/2020-12/schema',
    '$id': 'wiglenet_v3_detail',
    'title': 'wigle.net v3 detail response',
    'type': 'object',
    'required': [
        'bestClusterWiGLEQoS',
        'firstSeen',
        'lastSeen',
        'lastUpdate',
        'networkId',
        'trilateratedLatitude',
        'trilateratedLongitude',
        'type',
        'locationClusters',
    ],
    'properties': {
        'bestClusterWiGLEQoS': {'type': 'integer'},
        'channel': {'type': 'integer'},
        'firstSeen': {'type': 'string', 'pattern': time_re},
        'lastSeen': {'type': 'string', 'pattern': time_re},
        'lastUpdate': {'type': 'string', 'pattern': time_re},
        'networkId': {'type': 'string', 'pattern': mac_or_cell_re},
        'trilateratedLatitude': {
            'type': 'number', 'minimum': -90.0, 'maximum': 90.0},
        'trilateratedLongitude': {
            'type': 'number', 'minimum': -180.0, 'maximum': 180.0},
        # 'type': {'enum': type_enum},
        'type': {'type': ['null', 'string']},
        'xarfcn': {'type': ['null', 'string']},
        # 'attributes': {'type': 'array'},
        # 'streetAddress': {'type': 'object'},
        'locationClusters': {
            'type': 'array',
            'items': {
                'type': 'object',
                'required': [
                    'maxLastUpdate',
                    'minLastUpdate',
                    'score',
                    'locations',
                ],
                'properties': {
                    'centroidLatitude': {
                        'type': 'number', 'minimum': -90.0, 'maximum': 90.0},
                    'centroidLongitude': {
                        'type': 'number', 'minimum': -180.0, 'maximum': 180.0},
                    'clusterSsid': {'type': ['null', 'string']},
                    'daysObservedCount': {'type': 'integer'},
                    'maxLastUpdate': {'type': 'string', 'pattern': time_re},
                    'minLastUpdate': {'type': 'string', 'pattern': time_re},
                    'score': {'type': 'integer'},
                    'locations': {
                        'type': 'array',
                        'items': {
                            'type': 'object',
                            'required': ['latitude', 'longitude'],
                            'properties': {
                                'accuracy': {'type': 'number'},
                                'alt': {'type': 'integer'},
                                'attributes': {'type': ['null', 'string']},
                                'channel': {'type': 'integer'},
                                # 'genType': {'enum': type_enum},
                                'genType': {'type': ['null', 'string']},
                                'lastupdt': {'type': 'string', 'pattern': time_re},
                                'latitude': {
                                    'type': 'number',
                                    'minimum': -90.0,
                                    'maximum': 90.0
                                },
                                'longitude': {
                                    'type': 'number',
                                    'minimum': -180.0,
                                    'maximum': 180.0
                                },
                                'month': {'type': 'string'},
                                'name': {'type': ['null', 'string']},
                                'netid': {'type': 'string', 'pattern': mac_or_cell_re},
                                'signal': {'type': 'integer'},
                                'ssid': {'type': ['null', 'string']},
                                'time': {'type': 'string', 'pattern': time_re},
                            }
                        }
                    }
                }
            }
        }
    }
}
