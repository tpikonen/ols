"""wigle.net resolver for OLS."""
from .wiglenet import WiglenetCacheOnlyResolver, WiglenetResolver  # noqa: F401
