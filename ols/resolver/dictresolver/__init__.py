"""Resolver which uses a Python dict as data storage."""
from .dictresolver import DictResolver  # noqa: F401
