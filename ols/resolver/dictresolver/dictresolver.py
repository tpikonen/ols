from typing import Optional

from ols.utils import encode_cellarea, encode_cellid, encode_wifi
from ..base import ResolverBase


class DictResolver(ResolverBase):

    def __init__(self, dbdict):
        self.db = dbdict

    async def resolve_wifi(self, mac, **kwargs) -> Optional[dict]:
        return self.db.get(encode_wifi(mac))

    async def resolve_cell(self, tec, opc, lac, cid, **kwargs) -> Optional[dict]:
        return self.db.get(encode_cellid(tec, opc, lac, cid))

    async def resolve_cellarea(self, tec, opc, lac, **kwargs) -> Optional[dict]:
        return self.db.get(encode_cellarea(tec, opc, lac))

    async def resolve_bt(self, mac, **kwargs) -> Optional[dict]:
        return None
