from typing import Optional


class ResolverBase(object):
    """Base class for resolvers.

    Resolvers need to have at least:
     - 'name' instance attribute
     - At least one of async methods 'resolve_wifi', 'resolve_cell', 'resolve_cellarea'
       or 'resolve_bt'
    """

    async def resolve_wifi(self, mac, **kwargs) -> Optional[dict]:
        return None

    async def resolve_cell(self, tec, opc, lac, cid, **kwargs) -> Optional[dict]:
        return None

    async def resolve_cellarea(self, tec, opc, lac, **kwargs) -> Optional[dict]:
        return None

    async def resolve_bt(self, mac, **kwargs) -> Optional[dict]:
        return None
