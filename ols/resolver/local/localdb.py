import logging
import sqlite3
from typing import Optional, TypedDict


VERSION = 1
log = logging.getLogger(__name__)

# Zoom level for location tiles.
# z = 21 gives a ~27 m tile diagonal in the equator and ~9 m on lat = 70.0.
# Compare with WIFI_MIN_RADIUS = 10 m, i.e. 20 m diameter.
TILE_ZOOM = 21

Cluster = TypedDict('Cluster', {
    'netid': str,
    'valid': bool,
    'firstobs': int,  # uint from epoch
    'lastobs': int,  # uint from epoch
    'numtiles': int,
    'lat': float,
    'lon': float,
    'radius': float,
})

Location = TypedDict('Location', {
    'netid': str,
    'x': int,  # tile x
    'y': int,  # tile y, z is TILE_ZOOM
    'firstobs': int,  # uint from epoch
    'lastobs': int,  # uint from epoch
})


class LocalDB(object):

    def __init__(self, filename) -> None:
        self.con = sqlite3.connect(filename)
        self.con.row_factory = sqlite3.Row
        self.maybe_create()
        log.debug('Database file: ' + filename)
        log.debug('Database version: ' + str(self.get_version()))

    def get_version(self) -> int:
        cur = self.con.cursor()
        return cur.execute('PRAGMA user_version').fetchone()[0]

    def maybe_create(self):
        """DB with local observations with location."""
        version = self.get_version()
        if version != 0 and version != VERSION:
            log.error('Wrong version of the database detected, exiting')
            raise ValueError

        cur = self.con.cursor()

        # FIXME: Cellarea searches (the (rtype, opc, lac) tuple, see ichnaea) would
        # need either a separate netid type (like 'c_' + f'{rtype}_{opc}_{lac}') or
        # storing cell tuple items separately for cell locations and clusters,
        # so that the cell area could be efficiently extracted from cell rows.
        cur.execute("""
            CREATE TABLE IF NOT EXISTS cluster (
                -- # Sqlite creates a rowid automatically: rowid INTEGER
                netid TEXT, -- 'w_' + mac for wifi, rtype_opc_lac_cid for cell
                valid INTEGER, -- 1 for valid cluster, only one valid per netid allowed
                firstobs INTEGER, -- usec from epoch
                lastobs INTEGER, -- usec from epoch
                -- numobs INTEGER, -- number of observations in this cluster
                numtiles INTEGER, -- number of distinct tiles, numobs proxy in scoring
                lat REAL, -- calculated from locations
                lon REAL,
                radius REAL -- meters
                -- score INTEGER, -- ichnaea_score can be computed from other cols
            );
        """)
        cur.execute("""
            CREATE TABLE IF NOT EXISTS location (
                -- # Sqlite creates a rowid automatically: rowid INTEGER
                netid TEXT, -- mac for wifi/bt, opc_lac_cid tuple for cell
                x INTEGER, -- tile x
                y INTEGER, -- tile y, z is TILE_ZOOM
                firstobs INTEGER, -- usec from epoch
                lastobs INTEGER, -- usec from epoch
                -- numobs, -- number of observations in this tile
                PRIMARY KEY (netid, x, y)
            );
        """)
        cur.execute(f'PRAGMA user_version = {VERSION}')
        self.con.commit()

    def find_best_cluster(self, netid) -> Optional[Cluster]:
        cur = self.con.cursor()
        cs = cur.execute("""
            SELECT * FROM cluster WHERE netid = ? AND valid = 1;
            """, (netid,)).fetchall()
        clusters = [Cluster(e) for e in cs]
        if len(clusters) > 1:
            log.error("More than 1 valid clusters in DB!")
        return clusters[0] if clusters else None

    def find_clusters(self, netid: int) -> list[Cluster]:
        cur = self.con.cursor()
        cs = cur.execute("""
            SELECT * FROM cluster WHERE netid = ?;
            """, (netid,)).fetchall()
        return [Cluster(e) for e in cs]

    def delete_clusters(self, netid: str) -> None:
        """Delete all clusters with netid from DB."""
        cur = self.con.cursor()
        cur.execute("""
            DELETE FROM cluster WHERE netid = ?;
            """, (netid,))
        self.con.commit()

    def insert_cluster(self, cluster: Optional[Cluster]) -> None:
        """Insert cluster into DB, does not check for duplicates."""
        if cluster is None:
            return
        cur = self.con.cursor()
        cur.execute("""
            INSERT INTO cluster VALUES(
                :netid,
                :valid,
                :firstobs,
                :lastobs,
                :numtiles,
                :lat,
                :lon,
                :radius
            )
        """, cluster)
        self.con.commit()

    def list_location_netids(self) -> list[str]:
        cur = self.con.cursor()
        netids = cur.execute("""
            SELECT DISTINCT(netid) FROM location;
            """)
        return [n[0] for n in netids.fetchall()]

    def list_valid_cluster_netids(self) -> list[str]:
        cur = self.con.cursor()
        netids = cur.execute("""
            SELECT DISTINCT(netid) FROM cluster WHERE valid = 1;
            """)
        return [n[0] for n in netids.fetchall()]

    def list_invalid_cluster_netids(self) -> list[str]:
        cur = self.con.cursor()
        netids = cur.execute("""
            SELECT netid FROM cluster
            EXCEPT
            SELECT netid FROM cluster WHERE valid == 1;
            """)
        return [n[0] for n in netids.fetchall()]

    def find_locations(self, netid) -> list[Location]:
        cur = self.con.cursor()
        locs = cur.execute("""
            SELECT * FROM location WHERE netid = ?;
            """, (netid,)).fetchall()
        return [Location(e) for e in locs]

    def delete_location(self, location: Location) -> None:
        pass

    def insert_location(self, location: Location) -> bool:
        """Insert or update a location tile, tell if clustering needs updating.

        If the location tile for the netid does not exist in the DB, create
        a new one and return True.

        If it exists, update the timestamps firstobs and lastobs and return
        False.
        """
        # FIXME: This should also update the 'lastobs' timestamp of the
        # corresponding cluster
        cur = self.con.cursor()
        oldloc = cur.execute("""
            SELECT rowid, * FROM location WHERE netid = ? AND x = ? AND y = ?;
            """, (location["netid"], location["x"], location["y"])).fetchone()
        if oldloc is None:
            log.debug("INSERT location")
            cur.execute("""
                INSERT INTO location VALUES(
                    :netid,
                    :x,
                    :y,
                    :firstobs,
                    :lastobs
                )
            """, location)
            update_needed = True
        else:
            log.debug("UPDATE location")
            cur.execute("""
                UPDATE location
                SET firstobs = ?, lastobs = ?
                WHERE rowid = ?
            """, (min(location["firstobs"], oldloc["firstobs"]),
                  max(location["lastobs"], oldloc["lastobs"]),
                  oldloc["rowid"]))
            update_needed = False

        self.con.commit()
        return update_needed
