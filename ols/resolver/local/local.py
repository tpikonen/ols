import itertools
import logging
import sys
from datetime import datetime
from typing import Dict, List, Optional

# Use scipy.cluster.hierarchy for clustering / linkage,
# until we find a way to get rid of scipy
# import fastcluster
import numpy
from scipy.cluster import hierarchy

from .localdb import Cluster, LocalDB, Location, TILE_ZOOM
from ..base import ResolverBase
from ...constants import (
    BLUE_MAX_RADIUS,
    CELLAREA_MAX_RADIUS,
    CELL_MAX_RADIUS,
    MIN_CLUSTER_AGE,
    TEMPORARY_BLOCKLIST_DURATION,
    WIFI_DEFAULT_SIGNAL,
    WIFI_MAX_RADIUS,
    WIFI_MIN_SIGNAL,
)
from ...obsdb import UpdaterBase
from ...utils import (
    circle_radius,
    cut_from_linkage,
    encode_cellarea,
    encode_cellid,
    encode_wifi,
    haversine,
    ichnaea_score,
    mcc_mnc_to_opc,
    tile_bbox,
    tile_to_latlon,
    webmercator_tile,
)

log = logging.getLogger(__name__)

MIN_ACCURACY = 150  # m

_dbfile_to_resolver: Dict[str, object] = {}


def localdb_score(now, firstobs, lastobs, num_tiles):
    """Scoring for clusters is ichnaea score, but samples set to 2 * num_tiles."""
    samples = 2 * num_tiles
    return ichnaea_score(now, firstobs, lastobs, samples)


def get_localresolver(dbfile, name="Unknown LocalResolver"):
    if dbfile in _dbfile_to_resolver:
        return _dbfile_to_resolver[dbfile]
    else:
        resolver = LocalResolver(dbfile, name=name)
        _dbfile_to_resolver[dbfile] = resolver
        return resolver


class LocalResolver(ResolverBase, UpdaterBase):

    def __init__(self, dbfile, name="Unknown LocalResolver"):
        self.name = name
        self.db = LocalDB(dbfile)

    def insert_locate(self, observation, latlon, accuracy):
        """Insert observation from a (possibly resolved) locate API call."""
        if accuracy is None or accuracy > MIN_ACCURACY:
            return
        if latlon is None or None in latlon:
            return
        # FIXME: Convert 'time' to an ms since epoch 'timestamp' in observation
        timestamp = int(observation['time'].timestamp() * 1e6)  # us from epoch

        for wifi in observation.get('wifiAccessPoints') or []:
            strength = wifi.get('signalStrength', WIFI_DEFAULT_SIGNAL)
            if strength > WIFI_MIN_SIGNAL:
                netid = encode_wifi(wifi['macAddress'])
                self._add_network(netid, timestamp, latlon, accuracy)

        for c in observation.get('cellTowers') or []:
            opc = mcc_mnc_to_opc(c['mobileCountryCode'], c['mobileNetworkCode'])
            cell_netid = encode_cellid(
                c['radioType'], opc, c['locationAreaCode'], c['cellId'])
            self._add_network(cell_netid, timestamp, latlon, accuracy)
            cellarea_id = encode_cellarea(
                c['radioType'], opc, c['locationAreaCode'])
            self._add_network(cellarea_id, timestamp, latlon, accuracy)

    def insert_submit(self, submission):
        """Insert observation from a submit API call."""
        for item in submission['items']:
            # Submission timestamp is in ms since epoch
            timestamp = item.get('timestamp',
                                 int(datetime.now().timestamp()) * 1e3)
            # FIXME: insert_locate should use ms since epoch timestamp
            # like in submit schema
            item['time'] = datetime.fromtimestamp(timestamp / 1.0e3)

            pos = item.get('position')
            if pos is None:
                log.warning("Submission without position")
                return
            # lat and lon are validated in the schema, accuracy is not
            latlon = (pos['latitude'], pos['longitude'])
            accuracy = pos.get('accuracy', 10e3)
            self.insert_locate(item, latlon, accuracy)

    def _rerun_clustering(self):
        netids = self.db.list_location_netids()
        log.info(f"Have {len(netids)} locations")
        for i, netid in enumerate(netids):
            if i % 100 == 0:
                log.info(f"Location {i}/{len(netids)}: {netid}")
            self._recluster(netid)

    def _cluster_from_locations(self,
                                locations: list[Location],
                                valid: bool = False) -> Optional[Cluster]:
        if not locations:
            return None

        nw, se = tile_bbox(locations[0]['x'], locations[0]['y'], TILE_ZOOM)
        lat_corr = (se[0] - nw[0]) / 2.0
        lon_corr = (se[1] - nw[1]) / 2.0
        latlons = [tile_to_latlon(e['x'], e['y'], TILE_ZOOM) for e in locations]
        center = (numpy.mean([e[0] for e in latlons]) + lat_corr,
                  numpy.mean([e[1] for e in latlons]) + lon_corr)

        bb_nw = max(e[0] for e in latlons), min(e[1] for e in latlons)
        bb_se = (min(e[0] for e in latlons) + 2.0 * lat_corr,
                 max(e[1] for e in latlons) + 2.0 * lon_corr)
        radius = circle_radius(center, bb_nw, bb_se)

        cluster: Cluster = {
            'netid': locations[0]['netid'],
            'valid': valid,
            'firstobs': min(e['firstobs'] for e in locations),
            'lastobs': max(e['lastobs'] for e in locations),
            'numtiles': len(locations),
            'lat': center[0],
            'lon': center[1],
            'radius': radius,
        }
        return cluster

    def _recluster(self, netid: str) -> None:
        """Recalculate clustering for netid and update DB."""
        locations = self.db.find_locations(netid)
        length = len(locations)

        msg = f"Reclustering {netid}"

        self.db.delete_clusters(netid)
        if length < 1:
            log.debug(msg + ": no locations")
            return
        if length < 2:
            cluster = self._cluster_from_locations(locations, True)
            self.db.insert_cluster(cluster)
#            log.debug(msg + ": 1 location")
            return

        # TODO: Automatic max radius determination from data (discard outliers
        # etc.) for cell and cellarea nets
        if netid.startswith('w_'):
            max_cluster_radius = WIFI_MAX_RADIUS
        elif netid.startswith('b_'):
            max_cluster_radius = BLUE_MAX_RADIUS
        elif netid.startswith('c_'):
            max_cluster_radius = CELLAREA_MAX_RADIUS
        else:
            max_cluster_radius = CELL_MAX_RADIUS

        positions = [tile_to_latlon(loc['x'], loc['y'], TILE_ZOOM) for loc in locations]
        dist_matrix = numpy.zeros(length * (length - 1) // 2, dtype=numpy.double)
        for i, (a, b) in enumerate(itertools.combinations(positions, 2)):
            dist_matrix[i] = haversine(a, b)

        if dist_matrix.max() < max_cluster_radius:  # 1 cluster
            cluster = self._cluster_from_locations(locations, True)
            self.db.insert_cluster(cluster)
#            log.debug(msg + f": 1 cluster with {length} locations")
            return

        # More than 1 cluster
        # link_matrix = fastcluster.linkage(dist_matrix, method="complete")
        link_matrix = hierarchy.linkage(dist_matrix, method="complete")
        cut = cut_from_linkage(link_matrix, max_cluster_radius)
        n_clusters = max(cut) + 1

        # A netid where observations cluster into multiple groups is either
        # 1) mobile, so there will be no valid clusters,
        # 2) has one large observation cluster and one or more very small clusters
        #    which are not recent, likely caused by observation errors
        # 3) moved, but stable after moving, i.e. has more than one large clusters
        #    which are clearly separated in observation time
        # Being moved and valid requires that i) there is no
        # overlap in observation time range (firstobs to lastobs) between the
        # cluster with the latest observation and other valid clusters and that ii)
        # there is a multi-day (TEMPORARY_BLOCKLIST_DURATION) range of observations
        # in the latest cluster.

        groups: List[List[Location]] = [[] for _ in range(n_clusters)]
        for i, c in enumerate(cut):
            groups[c].append(locations[i])

        is_valid = []
        tranges = []
        lasttime = 0
        lastind = -1
        i = 0
        for i, group in enumerate(groups):
            mintime = min(e['firstobs'] for e in group)
            maxtime = max(e['lastobs'] for e in group)
            is_valid.append((maxtime - mintime) > MIN_CLUSTER_AGE)
            tranges.append((mintime, maxtime))
            if maxtime > lasttime:
                lasttime = maxtime
                lastind = i

        have_valid = (
            (lastind >= 0)
            # Cluster with latest observation has been observed long enough
            and ((tranges[lastind][1] - tranges[lastind][0])
                 > TEMPORARY_BLOCKLIST_DURATION)
            # Other valid clusters do not overlap in observation range
            and (max((tranges[i][1] for i in range(len(tranges))
                      if i != lastind and is_valid[i]),
                     default=0) < tranges[lastind][0]))

        # FIXME: Should maybe count the number of recent clusters and invalidate
        # if there are too many of them.

        # TODO: Don't store oldest clusters, if there are many newer clusters.
        # Also delete the old locations from DB.
        for i, group in enumerate(groups):
            cluster = self._cluster_from_locations(
                group, valid=(i == lastind and have_valid))
            self.db.insert_cluster(cluster)

        log.debug(msg + f": {n_clusters} clusters, valid = {have_valid}")

    def _add_network(self,
                     netid: str,
                     timestamp: int,  # usec from epoch
                     latlon: tuple[float, float],
                     accuracy: float) -> None:
        tile = webmercator_tile(latlon[0], latlon[1], TILE_ZOOM)
        location: Location = {
            "netid": netid,
            "x": tile.x,
            "y": tile.y,
            "firstobs": timestamp,
            "lastobs": timestamp,
        }
        if self.db.insert_location(location):
            log.debug(f"Reclustering {netid}")
            self._recluster(netid)

    def _resolve_netid(self, netid) -> Optional[dict]:
        cluster = self.db.find_best_cluster(netid)
        if cluster is None:
            return None

        now = int(datetime.now().timestamp() * 1e6)
        return {
            'latlon': (cluster['lat'], cluster['lon']),
            'radius': cluster['radius'],
            'age': now - cluster['lastobs'],
            'score': localdb_score(
                now, cluster['firstobs'], cluster['lastobs'], cluster['numtiles']),
        }

    async def resolve_wifi(self, mac, **kwargs) -> Optional[dict]:
        return self._resolve_netid(encode_wifi(mac))

    async def resolve_cell(self, tec, opc, lac, cid, **kwargs) -> Optional[dict]:
        cmodel = self._resolve_netid(encode_cellid(tec, opc, lac, cid))
        return (cmodel if cmodel is not None
                else self._resolve_netid(encode_cellarea(tec, opc, lac)))


def recluster_main():
    dbfile = sys.argv[1]
    lres = LocalResolver(dbfile)
    lres._rerun_clustering()
