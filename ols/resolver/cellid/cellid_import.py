import argparse
import csv
import gzip
import logging

from ols.resolver.cellid import CellIdWriter


logging.basicConfig(
    level=logging.DEBUG,
    format='[%(asctime)s.%(msecs)03d] %(levelname)s: %(message)s',
    datefmt='%Y-%m-%d %H:%M:%S')
log = logging.getLogger(__name__)


def int_or_none(s):
    try:
        return int(s)
    except (ValueError, TypeError):
        return None


def open_maybe_gzip(filename):
    with open(filename, 'rb') as f:
        is_gzip = (f.read(2) == b'\x1f\x8b')
    if is_gzip:
        return gzip.open(filename, 'rt')
    else:
        return open(filename, 'rt')


def main():
    parser = argparse.ArgumentParser(description=(
        'Import selected rows from cell id export CSV file to an sqlite DB file'))
    parser.add_argument('CSVFILE')
    parser.add_argument('-o', '--output', dest='DBFILE', required=True)
    parser.add_argument('-c', '--mcc', dest='MCC', required=False)
    parser.add_argument('-n', '--mnc', dest='MNC', required=False)
    args = parser.parse_args()

    db = CellIdWriter(args.DBFILE)

    mcc = int_or_none(args.MCC)
    mnc = int_or_none(args.MNC)

    log.info('Starting')
    rows = []
    with open_maybe_gzip(args.CSVFILE) as csvfile:
        has_header = csv.Sniffer().has_header(csvfile.read(1024))
        csvfile.seek(0)
        reader = csv.reader(csvfile, delimiter=',')
        if has_header:
            _ = next(reader)
        for ind, row in enumerate(reader):
            if (ind + 1) % 1e5 == 0:
                log.debug(f'On line {ind + 1}')
            if row is None:
                continue
            if mcc is not None and mcc != int(row[1]):
                continue
            if mnc is not None and mnc != int(row[2]):
                continue
            rows.append(row)

    log.info(f"Processed {ind} rows.")
    log.info(f"Found {len(rows)} matching rows.")
    db.insert_or_replace_csvrows(rows)
    log.info('Done')
