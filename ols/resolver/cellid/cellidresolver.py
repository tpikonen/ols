import logging
import os.path
import sqlite3
from datetime import datetime
from typing import Optional

from ..base import ResolverBase
from ...utils import ichnaea_score, opc_to_mcc_mnc

log = logging.getLogger(__name__)

DB_VERSION = 1


class CellIdResolver(ResolverBase):
    """Resolve cell towers from db with columns in the cell id export format.

    The database is read-only and can only be updated externally.

    Format description:
    https://ichnaea.readthedocs.io/en/latest/import_export.html
    """

    def __init__(self, dbfile, name="Unknown CellIdResolver") -> None:
        self.name = name
        if not os.path.isfile(dbfile):
            raise FileNotFoundError(f'cellid db file {dbfile} not found')
        log.debug(f'Using {dbfile=}')
        self.con = sqlite3.connect(dbfile)
        version = self.get_db_version()
        if version != 0 and version != DB_VERSION:
            log.error('Incompatible database version detected')
            raise ValueError
        self.con.row_factory = sqlite3.Row

    def get_db_version(self):
        cur = self.con.cursor()
        return cur.execute('PRAGMA user_version').fetchone()[0]

    def search_network(self, tec, mcc, net, area, cell) -> Optional[dict]:
        cur = self.con.cursor()
        # tec is not required in observations
        radio = tec.upper() if tec is not None else 'GSM'
        # log.debug(f'search_network: Got {radio} {mcc} {net} {area} {cell}')
        net = cur.execute("""
            SELECT * FROM cell
            WHERE radio = ? AND mcc = ? AND net = ? AND area = ? AND cell = ?;
            """, (radio, mcc, net, area, cell)).fetchone()
        if net is not None:
            return {k: net[k] for k in net.keys()}

        net = cur.execute("""
            SELECT * FROM cell WHERE mcc = ? AND net = ? AND area = ? AND cell = ?;
            """, (mcc, net, area, cell)).fetchone()
        if net is not None:
            return {k: net[k] for k in net.keys()}
        else:
            return None

    def _resolve_cell(self, tec, opc, lac, cid) -> Optional[dict]:
        mcc, mnc = opc_to_mcc_mnc(opc)
        if mcc is None:
            log.debug(f'opc to mcc,mnc conversion failed for {tec}_{opc}_{lac}_{cid}')
            return None
        netd = self.search_network(tec, mcc, mnc, lac, cid)
        if netd is None:
            log.debug(f'Resolving failed for {tec}_{opc}_{lac}_{cid}')
            return None
        log.debug(f'Resolving successful for {tec}_{opc}_{lac}_{cid}')
        now = int(datetime.now().timestamp() * 1e6)
        return {
            'latlon': (netd['lat'], netd['lon']),
            'radius': netd['range'],
            'age': now - netd['updated'] * 1e6,  # usec
            'score': ichnaea_score(
                now, netd['created'] * 1e6, netd['updated'] * 1e6, netd['samples']),
        }

    async def resolve_cell(self, tec, opc, lac, cid, **kwargs) -> Optional[dict]:
        return self._resolve_cell(tec.upper(), opc, lac, cid)

    async def resolve_wifi(self, mac, **kwargs) -> Optional[dict]:
        return None


def convert_csvrow(r):
    """Convert columns in opencellid format row and return necessary ones."""
    try:
        # Full row
        # row = (r[0], int(r[1]), int(r[2]), int(r[3]), int(r[4]), int_or_none(r[5]),
        #        float(r[6]), float(r[7]), int(r[8]), int(r[9]), int(r[10]),
        #        int(r[11]), int(r[12]), int_or_none(r[13]))
        # Selected items
        row = (r[0], int(r[1]), int(r[2]), int(r[3]), int(r[4]),
               float(r[6]), float(r[7]), int(r[8]), int(r[10]),
               int(r[11]), int(r[12]))
        return row
    except ValueError as e:
        log.error(f'Error converting row: {r}')
        raise e


class CellIdWriter(object):
    """Creates a CellId DB used by CellIdResolver."""

    def __init__(self, dbfile) -> None:
        self.con = sqlite3.connect(dbfile)
        self.maybe_create_db()
        log.debug('Database version: ' + str(self.get_db_version()))
        self.con.row_factory = sqlite3.Row

    def get_db_version(self):
        cur = self.con.cursor()
        return cur.execute('PRAGMA user_version').fetchone()[0]

    def maybe_create_db(self):
        version = self.get_db_version()
        if version != 0 and version != DB_VERSION:
            log.error('Incompatible database version detected')
            raise ValueError

        cur = self.con.cursor()
        cur.execute("""
            CREATE TABLE IF NOT EXISTS cell (
                radio TEXT NOT NULL,
                mcc INTEGER NOT NULL,
                net INTEGER NOT NULL,
                area INTEGER NOT NULL,
                cell INTEGER NOT NULL,
                lon REAL NOT NULL,
                lat REAL NOT NULL,
                range INTEGER,
                samples INTEGER,
                created INTEGER,
                updated INTEGER,
                PRIMARY KEY (radio, mcc, net, area, cell)
            );
            """)
        cur.execute(f'PRAGMA user_version = {DB_VERSION}')
        self.con.commit()

    def insert_or_replace_rows(self, rows):
        self.con.executemany("""
            INSERT OR REPLACE INTO cell VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);
            """, rows)
        self.con.commit()

    def insert_or_replace_csvrows(self, csvrows):
        self.insert_or_replace_rows([convert_csvrow(r) for r in csvrows])
