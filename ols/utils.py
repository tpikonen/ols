from math import asin, cos, log, radians, sin, sqrt
from typing import Optional

import mercantile  # type: ignore


def clusters_from_linkage(links):
    """Return cluster memberships as a list of lists for each non-singleton cluster.

    Linkage matrix links is from fastcluster or scipy.cluster.hierarchy.linkage.

    For example, a clustering of 4 elements could result in a linkage matrix
    with 2 rows, i.e. the full linkage has 6 clusters, for example
    [[0], [1], [2], [3], [0, 1], [2, 3]]. The first 4 clusters are the trivial
    singleton clusters which are omitted, so the returned value is
    [[0, 1], [2, 3]].
    """
    length = int(links[-1, -1])  # number of singleton clusters

    def is_singleton(n):
        """Return True if index points to a singleton cluster."""
        return n < length

    clusters = [[n] for n in range(length)] + [
        [int(links[m, 0]), int(links[m, 1])] for m in range(links.shape[0])]

    all_resolved = False
    while not all_resolved:
        all_resolved = True
        for ci in range(len(clusters)):
            if not all(is_singleton(index) for index in clusters[ci]):
                all_resolved = False
                newcluster = []
                for n in clusters[ci]:
                    if is_singleton(n):
                        newcluster.append(n)
                    else:
                        newcluster.extend(clusters[n])
                clusters[ci] = newcluster

    return clusters[length:]  # omit singletons


def cut_from_linkage(links, level):
    """Given a linkage matrix and a height level, return flat clusters.

    Linkage matrix is from fastcluster or scipy.cluster.hierarchy.linkage.

    Like scipy.clustering.hierarchy.cut_tree with height == level.
    """
    assert level >= 0.0
    length = int(links[-1, -1])  # number of singleton clusters
    clusters = clusters_from_linkage(links)
    levels = [0.0] * length + [links[m, 2] for m in range(links.shape[0])]

    included = [length + len(clusters) - 1]  # last cluster with all elements
    all_resolved = False
    while not all_resolved:
        all_resolved = True
        new_included = []
        for cind in included:
            if levels[cind] < level:
                new_included.append(cind)
            else:
                new_included.extend(int(x) for x in links[cind - length, 0:2])
                all_resolved = False

        included = new_included

    out = [-1] * length
    for i, cind in enumerate(included):
        elems = clusters[cind - length] if cind >= length else [cind]
        for eind in elems:
            out[eind] = i

    assert all(e >= 0 for e in out)
    return out


# Haversine function nicked from:
# https://stackoverflow.com/questions/4913349/haversine-formula-in-python-bearing-and-distance-between-two-gps-points
def haversine(p1, p2):
    """Return the great circle distance between two latlon points on earth.

    The latlon must be in decimal degrees, the distance is calculated in
    meters.
    """
    (lat1, lon1) = (p1[0], p1[1])
    (lat2, lon2) = (p2[0], p2[1])
    # convert decimal degrees to radians
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])

    # haversine formula
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    a = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlon / 2) ** 2
    c = 2 * asin(sqrt(a))
    r = 6371 * 1000  # Radius of earth in meters
    return c * r


def radius_estimator(ranges, level=0.9):
    """Estimate the effective station radius from an iterable of ranges.

    Tries to get rid of long range outliers. The estimate is the 90th
    percentile value of the sorted ranges, if enough ranges are supplied.
    """
    values = list(ranges).sort(reverse=True)
    if len(values) <= 3:
        return values[0]
    else:
        return values[max(int(len(values) * level), 1)]


def circle_radius(center, north_west, south_east):
    """Return largest distance from center to corners of a bounding box.

    The bounding box is defined by its north west and southeast points, given
    as latlon tuples.

    Used as radius in Ichnaea.
    """
    return max(haversine(center, p) for p in (
        north_west,
        (north_west[0], south_east[1]),
        (south_east[0], north_west[1]),
        south_east))


def ichnaea_score(now, created, last_seen, samples):
    """Return a score as a floating point number.

    The score represents the quality or trustworthiness of this record.
    This is similar to (i.e. copied from) the scoring in Ichnaea.
    """
    usec_per_day = 24 * 60 * 60 * 1e6

    # age_weight is a number between:
    # 1.0 (data from last month) to
    # 0.277 (data from a year ago)
    # 0.2 (data from two years ago)
    # month_old = max((now - obj.modified).days, 0) // 30
    month_old = max(now - last_seen, 0) // (30 * usec_per_day)
    age_weight = 1 / sqrt(month_old + 1)

    # collection_weight is a number between:
    # 0.1 (data was only seen on a single day)
    # 0.2 (data was seen on two different days)
    # 1.0 (data was first and last seen at least 10 days apart)
    collected_over = max((last_seen - created) // usec_per_day, 1)
    collection_weight = min(collected_over / 10.0, 1.0)

    # sample_weight is a number between:
    # 0.5 for 1 sample
    # 1.0 for 2 samples
    # 3.32 for 10 samples
    # 6.64 for 100 samples
    # 10.0 for 1024 samples or more
    sample_weight = min(max(log(max(samples, 1), 2), 0.5), 10.0)

    return age_weight * collection_weight * sample_weight


def webmercator_tile(lat, lon, z):
    """Return the mercantile.Tile containing the given lat, lon point at zoom z.

    This just calls mercantile.tile() with correct argument order.
    """
    return mercantile.tile(lon, lat, z)


def tile_to_latlon(x, y, z):
    lnglat = mercantile.ul(x, y, z)
    return lnglat[1], lnglat[0]


def tile_bbox(x, y, z):
    """Return northwest and southeast corners of a tile."""
    b = mercantile.bounds((x, y, z))
    return (b.north, b.west), (b.south, b.east)


def min_or_none(seq: list[Optional[int]]) -> Optional[int]:
    if any(x is None for x in seq):
        return None
    else:
        return min(seq)  # type: ignore


def opc_to_mcc_mnc(opc: str) -> (Optional[int], Optional[int]):
    """Return (mcc, mnc) integer tuple from opc string."""
    mcclen = 3
    try:
        mcc = int(opc[:mcclen])
        mnc = int(opc[mcclen:])
    except ValueError:
        mcc = None
        mnc = None

    return mcc, mnc


def mcc_mnc_to_opc(mcc: int, mnc: int) -> str:
    """Return operator code (OPC) string formed from MCC and MNC given as ints.

    MCC and MNC are actually strings, but Ichnaea API treats them as ints,
    so we have to deal with that.

    MCC is a 3 digit string. MNC is a string with 2 or more digits.
    MNC 5 becomes '05', not '005', since 3+ digit strings starting with '0' are
    not used in practise, see
    https://en.wikipedia.org/wiki/Mobile_network_codes_in_ITU_region_2xx_(Europe)

    The return value OPC is a 5+ digit string.
    """
    return f'{mcc:03}{mnc:02}'


def encode_cellid(tec: str, opc: str, lac: int, cid: int, **kwargs) -> str:
    return f'{tec.upper()}_{opc}_{lac}_{cid}'


def encode_cellarea(tec: str, opc: str, lac: int, **kwargs) -> str:
    return f'c_{tec.upper()}_{opc}_{lac}'


def encode_wifi(mac: str) -> str:
    return f'w_{mac.lower()}'
