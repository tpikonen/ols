geolocate_v1_valid_response_schema = {
    '$schema': 'https://json-schema.org/draft/2020-12/schema',
    '$id': 'https://ichnaea.readthedocs.io/en/latest/api/geolocate.html',
    'title': 'MLS v1/geolocate JSON API location response',
    'type': 'object',
    'properties': {
        'location': {
            'type': 'object',
            'properties': {
                'lat': {'type': 'number', 'minimum': -90.0, 'maximum': 90.0},
                'lng': {'type': 'number', 'minimum': -180.0, 'maximum': 180.0},
            },
            'required': ['lat', 'lng'],
        },
        'accuracy': {'type': 'number', 'minimum': 0.0},
    },
    'required': ['location'],
}

geolocate_v1_error_schema = {
    '$schema': 'https://json-schema.org/draft/2020-12/schema',
    '$id': 'https://ichnaea.readthedocs.io/en/latest/api/geolocate.html',
    'title': 'MLS v1/geolocate JSON API error response',
    'type': 'object',
    'properties': {
        'error': {
            'type': 'object',
            'properties': {
                'errors': {
                    'type': 'array',
                    'items': {
                        'type': 'object',
                        'properties': {
                            'domain': {'type': 'string'},
                            'reason': {'type': 'string'},
                            'message': {'type': 'string'},
                        },
                    },
                },
                'code': {
                    'type': 'integer',
                    'description': 'HTTP status code',
                    'minimum': 400,
                },
                'message': {
                    'type': 'string',
                    'description': 'Value "Not found"'
                },
            }
        }
    }
}

mac_re = r'^(?:[0-9a-fA-F]{2}:){5}[0-9a-fA-F]{2}$'
radiotype_enum = ['gsm', 'cdma', 'wcdma', 'lte', 'nr']

geolocate_v1_schema = {
    '$schema': 'https://json-schema.org/draft/2020-12/schema',
    '$id': 'https://ichnaea.readthedocs.io/en/latest/api/geolocate.html',
    'title': 'MLS v1/geolocate JSON API',
    'type': 'object',
    'properties': {
        'time': {'type': 'string', 'description': 'OLS extension'},
        'carrier': {'type': 'string'},
        'considerIp': {'type': 'boolean'},
        'homeMobileCountryCode': {'type': 'integer'},
        'homeMobileNetworkCode': {'type': 'integer'},
        'radioType': {'enum': radiotype_enum},
        'bluetoothBeacons': {
            'type': 'array',
            'items': {
                'type': 'object',
                'properties': {
                    'macAddress': {'type': 'string', 'pattern': mac_re},
                    'name': {'type': 'string'},
                    'age': {'type': 'string'},
                    'signalStrength': {'type': 'integer'},
                },
                'required': ['macAddress'],
            },
        },
        'cellTowers': {
            'type': 'array',
            'items': {
                'type': 'object',
                'properties': {
                    'radioType': {'enum': radiotype_enum},
                    'mobileCountryCode': {'type': 'integer', 'minimum': 0},
                    'mobileNetworkCode': {'type': 'integer', 'minimum': 0},
                    'locationAreaCode': {'type': 'integer', 'minimum': 0},
                    'cellId': {'type': 'integer', 'minimum': 0},
                    'newRadioCellId': {'type': 'integer', 'minimum': 0},
                    'age': {'type': 'integer', 'minimum': 0},
                    'psc': {'type': 'integer'},
                    'signalStrength': {'type': 'number'},
                    'timingAdvance': {'type': 'number'}},
                'required': [
                    'mobileCountryCode',
                    'mobileNetworkCode',
                    'locationAreaCode',
                    'cellId',
                ],
            }
        },
        'wifiAccessPoints': {
            'type': 'array',
            'items': {
                'type': 'object',
                'properties': {
                    'macAddress': {'type': 'string', 'pattern': mac_re},
                    'age': {'type': 'integer', 'minimum': 0},
                    'channel': {'type': 'integer', 'minimum': 0},
                    'frequency': {'type': 'number', 'minimum': 0},
                    'signalStrength': {'type': 'number'},
                    'signalToNoiseRatio': {'type': 'number'}},
                'required': ['macAddress'],
            },
        },
        'fallbacks': {
            'type': 'object',
            'properties': {
                'lacf': {'type': 'boolean'},
                'ipf': {'type': 'boolean'},
            }
        }
    }
}

source_enum = ['gps', 'manual', 'fused']

geosubmit_v2_schema = {
    '$schema': 'https://json-schema.org/draft/2020-12/schema',
    '$id': 'https://ichnaea.readthedocs.io/en/latest/api/geosubmit2.html',
    'title': 'MLS v2/geosubmit JSON API',
    'type': 'object',
    'required': ['items'],
    'properties': {
        'items': {
            'type': 'array',
            'items': {
                'type': 'object',
                'properties': {
                    'timestamp': {'type': 'integer', 'minimum': 0},
                    'position': {
                        'type': 'object',
                        'properties': {
                            'latitude': {
                                'type': 'number', 'minimum': -90.0, 'maximum': 90.0},
                            'longitude': {
                                'type': 'number', 'minimum': -180.0, 'maximum': 180.0},
                            'accuracy': {'type': 'number', 'minimum': 0.0},
                            'altitude': {'type': 'number'},
                            'altitudeAccuracy': {'type': 'number'},
                            'age': {'type': 'integer', 'minimum': 0},
                            'heading': {
                                'type': 'number', 'minimum': 0.0, 'maximum': 360.0},
                            'pressure': {'type': 'number', 'minimum': 0.0},
                            'speed': {'type': 'number', 'minimum': 0.0},
                            'source': {'enum': source_enum},
                        },
                        'required': ['latitude', 'longitude'],
                    },
                    'bluetoothBeacons': {
                        'type': 'array',
                        'items': {
                            'type': 'object',
                            'properties': {
                                'macAddress': {'type': 'string', 'pattern': mac_re},
                                'age': {'type': 'integer', 'minimum': 0},
                                'name': {'type': 'string'},
                                'signalStrength': {'type': 'integer'},
                            },
                            'required': ['macAddress'],
                        },
                    },
                    'cellTowers': {
                        'type': 'array',
                        'items': {
                            'type': 'object',
                            'properties': {
                                'radioType': {'enum': radiotype_enum},
                                'mobileCountryCode': {'type': 'integer', 'minimum': 0},
                                'mobileNetworkCode': {'type': 'integer', 'minimum': 0},
                                'locationAreaCode': {'type': 'integer', 'minimum': 0},
                                'cellId': {'type': 'integer', 'minimum': 0},
                                'newRadioCellId': {'type': 'integer', 'minimum': 0},
                                'age': {'type': 'integer', 'minimum': 0},
                                'asu': {'type': 'integer'},
                                'primaryScramblingCode': {'type': 'integer'},
                                'serving': {
                                    'type': 'integer', 'minimum': 0, 'maximum': 1},
                                'signalStrength': {'type': 'number'},
                                'timingAdvance': {'type': 'number'}},
                            'required': [
                                'mobileCountryCode',
                                'mobileNetworkCode',
                                'locationAreaCode',
                                'cellId',
                            ],
                        }
                    },
                    'wifiAccessPoints': {
                        'type': 'array',
                        'items': {
                            'type': 'object',
                            'properties': {
                                'macAddress': {'type': 'string', 'pattern': mac_re},
                                'age': {'type': 'integer', 'minimum': 0},
                                'channel': {'type': 'integer', 'minimum': 0},
                                'frequency': {'type': 'number', 'minimum': 0},
                                'signalStrength': {'type': 'number'},
                                'signalToNoiseRatio': {'type': 'number'}},
                            'required': ['macAddress'],
                        },
                    },
                },
            },
        },
    },
}
