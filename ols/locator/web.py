import asyncio
import logging
import os.path
import sys
from datetime import datetime

import aiohttp
import fastjsonschema  # type: ignore

from .base import LocatorBase
from .. import __version__
from ..schemas import (
    geolocate_v1_error_schema,
    geolocate_v1_valid_response_schema,
)

log = logging.getLogger(__name__)


class WebLocator(LocatorBase):

    def __init__(self, locateurl, name="Unknown WebLocator"):
        self.name = name
        self.api = locateurl
        self.response_ok_validate = fastjsonschema.compile(
            geolocate_v1_valid_response_schema)
        self.response_error_validate = fastjsonschema.compile(
            geolocate_v1_error_schema)
        log.debug(f"Using locateurl {locateurl}")

    async def locate(self, observation, **kwargs):
        jd = observation.copy()
        jd.pop('time', None)
        if 'cellTowers' in jd:
            jd['radioType'] = jd.get('radioType', 'GSM').lower()
            for obs in jd['cellTowers']:
                obs['radioType'] = obs.get('radioType', jd['radioType']).lower()

        headers = [('User-Agent', f'OLS/{__version__}')]
        async with aiohttp.ClientSession(headers=headers) as session:
            try:
                async with session.post(self.api, json=jd) as response:
                    content_type = response.headers['content-type']
                    if content_type:
                        content_type = content_type.split(';')[0]
                    if (response.status == 200
                            and content_type == 'application/json'):
                        try:
                            unvalidated = await response.json()
                            located = self.response_ok_validate(unvalidated)
                        except fastjsonschema.JsonSchemaValueException as e:
                            log.debug(unvalidated)
                            raise ValueError(
                                'Invalid locate response: ' + str(e))
                    elif (response.status >= 400
                            and content_type == 'application/json'):
                        try:
                            unvalidated = await response.json()
                            errord = self.response_error_validate(unvalidated)
                        except fastjsonschema.JsonSchemaValueException as e:
                            log.debug(unvalidated)
                            raise ValueError(
                                'Invalid error response: ' + str(e))
                        raise ValueError(
                            'Received an error: ' + errord['error']['message'])
                    else:
                        log.debug(await response.text())
                        raise ValueError(
                            'Unknown response with status code %d '
                            'and content-type %s' % (response.status, content_type))

            except aiohttp.ClientError as e:
                log.warning('Failed to get location: ' + str(e))
                return None, None

        loc = located.get('location', {})
        latlon = loc.get('lat'), loc.get('lng')
        accuracy = located.get('accuracy')
        log.debug(f"Got result: {latlon}, {accuracy}")
        return latlon, accuracy


async def query(observations, locateurl):
    retval = []
    locator = WebLocator(locateurl)
    for obs in observations:
        latlon, acc = await locator.locate(obs)
        retval.append({
            'time': obs['time'],
            'latlon': latlon,
        })
        await asyncio.sleep(1)
    sys.stderr.write('\n')
    sys.stderr.flush()
    return retval


def main(obfilename, gpxfilename):
    import gpxpy
    import yaml
    if os.path.exists(gpxfilename):
        print(f"Output file '{gpxfilename}' exists, exiting.")
        sys.exit(1)

    with open(obfilename, 'r') as fin:
        observations = yaml.safe_load(fin.read())

    print(f'Read {len(observations)} observations')
    # FIXME: Get config
    result = asyncio.run(query(observations))

    gpx = gpxpy.gpx.GPX()
    gpx_track = gpxpy.gpx.GPXTrack()
    gpx.tracks.append(gpx_track)
    gpx_segment = gpxpy.gpx.GPXTrackSegment()
    gpx_track.segments.append(gpx_segment)

    for item in result:
        latlon = item.get('latlon', (0.0, 0.0))
        t = item.get('time')
        time = (t if isinstance(t, datetime)
                else (datetime.fromisoformat(t)
                      if isinstance(t, str) else None))
        gpx_segment.points.append(gpxpy.gpx.GPXTrackPoint(
            latlon[0], latlon[1],
            time=time))

    with open(gpxfilename, 'w') as fout:
        fout.write(gpx.to_xml('1.0'))


if __name__ == '__main__':
    main(sys.argv[1], sys.argv[2])
