

class LocatorBase(object):
    """Base class for locators.

    Locators need to have at least:
     - 'name' instance attribute
     - 'locate' async method
    """

    async def locate(self, observation, hint=None, **kwargs):
        raise NotImplementedError
