import logging
import random

from .base import LocatorBase
from ..constants import (
    CELL_DEFAULT_RADIUS,
    CELL_MIN_SIGNAL,
    WIFI_DEFAULT_SIGNAL,
    WIFI_MAX_ACCURACY,
    WIFI_MAX_SIGNAL,
    WIFI_MIN_ACCURACY,
    WIFI_MIN_SIGNAL,
)
from ..utils import mcc_mnc_to_opc

log = logging.getLogger(__name__)


class SingleWifiLocatorBase(LocatorBase):
    """Base class to locate to the coordinates of a single wifi mac."""

    def __init__(self, wifiresolvers, name="Unknown SingleWifiLocatorBase"):
        self.name = name
        self.resolvers = wifiresolvers

    async def _locate_single_wifi(self, mac, strength=None, hint=None):
        if mac is None:
            return None, None

        signalstrength = strength or WIFI_DEFAULT_SIGNAL

        latlon, accuracy = None, None
        for resolver in self.resolvers:
            wifi = await resolver.resolve_wifi(mac, hint=hint)
            if wifi is not None:
                log.debug(f'Got result from {mac=}')
                latlon = wifi.get('latlon')
                sig = min(signalstrength, WIFI_MAX_SIGNAL)
                ratio = 1.0 - ((sig - WIFI_MIN_SIGNAL)
                               / (WIFI_MAX_SIGNAL - WIFI_MIN_SIGNAL))
                accuracy = max(min(wifi['radius'] * ratio, WIFI_MAX_ACCURACY),
                               WIFI_MIN_ACCURACY)
                break

        return latlon, accuracy


class FirstWifiLocator(SingleWifiLocatorBase):
    """Locate to the coordinates of the first found wifi in observations.

    Reorder the wifi observation list by the given function first.
    """

    def __init__(
            self, wifiresolvers, reorder=lambda x: x, name="Unknown FirstWifiLocator"):
        self.name = name
        self.resolvers = wifiresolvers
        self.reorder = reorder

    async def locate(self, observation, hint=None, **kwargs):
        wifiobs = self.reorder(observation.get('wifiAccessPoints', []))

        for wob in wifiobs:
            latlon, acc = await self._locate_single_wifi(
                wob.get('macAddress'), wob.get('signalStrength'), hint=hint)
            if latlon is not None:
                return latlon, acc

        return None, None


class StrongestWifiLocator(FirstWifiLocator):
    """Locate to the coordinates of the highest signal wifi in the observation."""

    def __init__(self, wifiresolvers, name="Unknown StrongestWifiLocator"):
        self.name = name

        def sort_by_strength(x):
            return sorted(
                x, reverse=True,
                key=lambda x: x.get('signalStrength', WIFI_DEFAULT_SIGNAL))

        super().__init__(wifiresolvers, sort_by_strength)


class RandomWifiLocator(FirstWifiLocator):
    """Locate to the coordinates of a random wifi in the observation."""

    def __init__(self, wifiresolvers, name="Unknown RandomWifiLocator"):
        self.name = name

        def shuffled(x):
            random.shuffle(x)
            return x

        super().__init__(wifiresolvers, shuffled)


class SingleCellLocatorBase(LocatorBase):
    """Base class to locate to the coordinates of a single cell."""

    def __init__(self, cellresolvers, name="Unknown SingleCellLocatorBase"):
        self.name = name
        self.resolvers = cellresolvers

    async def _locate_single_cell(self, rat, opc, lac, cid, strength=None, hint=None):
        if any(p is None for p in (rat, opc, lac, cid)):
            return None, None

        latlon, accuracy = None, None
        for resolver in self.resolvers:
            cell = await resolver.resolve_cell(rat, opc, lac, cid, hint=hint)
            if cell is not None:
                log.debug(f'Got result from {rat} {opc}_{lac}_{cid}')
                latlon = cell.get('latlon')
                accuracy = CELL_DEFAULT_RADIUS
                break

        return latlon, accuracy


class FirstCellLocator(SingleCellLocatorBase):
    """Locate to the coordinates of the first found cell in observations.

    Reorder the cell observation list by the given function first.
    """

    def __init__(
            self, cellresolvers, reorder=lambda x: x, name="Unknown FirstCellLocator"):
        self.name = name
        self.resolvers = cellresolvers
        self.reorder = reorder

    async def locate(self, observation, hint=None, **kwargs):
        cellobs = self.reorder(observation.get('cellTowers', []))

        for obs in cellobs:
            rat = obs['radioType']
            opc = mcc_mnc_to_opc(obs['mobileCountryCode'], obs['mobileNetworkCode'])
            lac = obs['locationAreaCode']
            cid = obs['cellId']
            latlon, acc = await self._locate_single_cell(
                rat, opc, lac, cid, strength=obs.get('signalStrength'), hint=hint)
            if latlon is not None:
                return latlon, acc

        return None, None


class StrongestCellLocator(FirstCellLocator):
    """Locate to the coordinates of the highest signal cell in the observation."""

    def __init__(self, cellresolvers, name="Unknown StrongestCellLocator"):
        def sort_by_strength(x):
            return sorted(
                x, reverse=True,
                key=lambda x: x.get('signalStrength', CELL_MIN_SIGNAL['GSM']))

        super().__init__(cellresolvers, reorder=sort_by_strength, name=name)
