import logging
import os
import sys
from collections import defaultdict
from statistics import mean

import mgrs
import msgpack
import siphashc as siphash

from .base import LocatorBase
from ..constants import WIFI_DEFAULT_SIGNAL
from ..m8bfile import CachedMapping, M8bSetMapping
from ..utils import haversine

log = logging.getLogger(__name__)
M = mgrs.MGRS()
sqrt2 = 1.4142135623730951


def mac2bytes(macstring, reverse=False):
    hexlist = macstring.split(':')
    if reverse:
        hexlist.reverse()
    return bytes(int(x, base=16) for x in hexlist)


def hashmac(macstring, bits=32):
    bmac = mac2bytes(macstring)
    zerokey = bytes([0] * 16)
    sip = siphash.siphash(zerokey, bmac)
#    print(f"sip: {sip}")
    mask = (1 << bits) - 1
    netid = sip & mask
#    print(f"netid: {netid}")
    return netid


def mgrs_to_latlon(mstr):
    """Convert the 4 digit MGRS square to coordinates in the center of the square."""
    zone_and_square = mstr[:5]
    grid2x2 = mstr[5:9]
    easting = grid2x2[:2]
    northing = grid2x2[2:]
    grid5x5 = easting + "500" + northing + "500"
    return M.toLatLon(zone_and_square + grid5x5)


class M8BLocator(LocatorBase):

    def __init__(self, filename, name="Unknown M8BLocator"):
        """Load and unpack msgpack containing m8b data from 'filename'."""
        self.name = name
        self.m8b = None
        self.basename = os.path.basename(filename)
        try:
            self.m8b = CachedMapping(M8bSetMapping(filename))
            log.debug(f"Opened native m8b file '{filename}'")
        except ValueError:
            with open(filename, 'rb') as fin:
                self.m8b = msgpack.load(fin, strict_map_key=False)
                log.debug(f"Opened m8b msgpack '{filename}'")

    def lookup(self, macstring):
        return self.m8b.get(hashmac(macstring))

    def query(self, macs, allhits=False):
        hdict = defaultdict(int)
        for mac in macs:
            coords = self.m8b.get(hashmac(mac), [])
            for c in coords:
                hdict[c] += 1
        hist = list(hdict.items())
        hist.sort(key=lambda x: x[1], reverse=True)
        if allhits:
            return hist
        elif len(macs) > 1:
            return [x for x in hist if x[1] > 1]
        else:
            return hist

    def locate_pop(self, observations):
        """Locate with m8b, resolve ties by removing low-signal macs.

        In case of ties, remove the lowest signal strength mac and retry.
        Does not really work.
        """
        macs = [obs[0] for obs in observations]
        while True:
            hist = self.query(macs, allhits=True)
            if not hist:
                return None
            numhits = hist[0][1]
            num_equals = len([1 for h in hist if h[1] >= numhits])
            print(f'Got {num_equals} equal hits from {len(macs)} macs')
            print(hist[:num_equals])
            if num_equals == 1:
                break
            elif len(macs) < 3:
                return None
            macs.pop()

        return mgrs_to_latlon(hist[0][0]), 700.0

    def locate_bbox(self, observations):
        """Locate with m8b, resolve ties by averaging locations.

        In case of ties, return location as mean latlon and accuracy as the
        longer edge of the bounding box of tied locations.
        """
        macs = [obs[0] for obs in observations]
        hist = self.query(macs, allhits=True)
        if not hist:
            log.debug(f"No matches from '{self.basename}'")
            return None, None
        maxhits = hist[0][1]
        log.debug(f"Best match from '{self.basename}' found "
                  f"{maxhits} of {len(macs)} observed APs")
        latlons = [mgrs_to_latlon(x[0]) for x in hist if x[1] >= maxhits]
        minlatlon = min(x[0] for x in latlons), min(x[1] for x in latlons)
        maxlatlon = max(x[0] for x in latlons), max(x[1] for x in latlons)
        meanlatlon = mean(x[0] for x in latlons), mean(x[1] for x in latlons)
        accuracy = round(0.5 * sqrt2 * max(
            haversine(minlatlon, (minlatlon[0], maxlatlon[1])),
            haversine(minlatlon, (maxlatlon[0], minlatlon[1])),
            1000.0))
        return meanlatlon, accuracy

    def locate_nonasync(self, *args, **kwargs):
        return self.locate_bbox(*args, **kwargs)

    async def locate(self, data, **kwargs):
        observations = [
            (d['macAddress'],
             d['signalStrength']
                if 'signalStrength' in d else WIFI_DEFAULT_SIGNAL)
            for d in data.get('wifiAccessPoints', []) if 'macAddress' in d]
        observations.sort(key=lambda obs: obs[1])
        return self.locate_bbox(observations)


def main():
    filename = sys.argv[1]
    macs = sys.argv[2:]
    loc = M8BLocator(filename)
    latlon, accuracy = loc.locate_nonasync(macs)
    print(f'{latlon}, {accuracy}')


if __name__ == '__main__':
    main()
