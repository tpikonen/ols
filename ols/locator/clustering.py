# Clustering locator based on Mozilla Ichnaea code.
# Ichnaea is offered under the Apache License 2.0.

import itertools
import logging
import math
from collections import defaultdict
from datetime import datetime, timezone

import numpy

from ols.constants import (
    CELL_MAX_ACCURACY,
    CELL_MIN_ACCURACY,
    CELL_MIN_SIGNAL,
    WIFI_DEFAULT_SIGNAL,
    WIFI_MAX_ACCURACY,
    WIFI_MAX_CLUSTER_SIZE,
    WIFI_MAX_RADIUS,
    WIFI_MAX_SIGNAL,
    WIFI_MIN_ACCURACY,
    WIFI_MIN_SIGNAL,
)
from ols.locator.base import LocatorBase
from ols.utils import encode_cellarea, encode_cellid, haversine, mcc_mnc_to_opc

from scipy.cluster import hierarchy
from scipy.optimize import leastsq

log = logging.getLogger(__name__)

NETWORK_DTYPE = numpy.dtype(
    [
        ("lat", numpy.double),
        ("lon", numpy.double),
        ("radius", numpy.double),
        ("age", numpy.int32),
        ("signalStrength", numpy.int32),
        ("score", numpy.double),
        ("netid", "S18"),
        ("seen_today", numpy.bool_),
    ]
)


class Dummy:
    """A class for objects with arbitrary attributes."""

    def __init__(self, d):
        self.__dict__.update(d)

    def update(self, d):
        self.__dict__.update(d)

    def __repr__(self):
        """Return the repr of the dict."""
        return self.__dict__.__repr__()


def utcnow():
    """Return the current time in UTC with a UTC timezone set."""
    return datetime.now(timezone.utc).replace(microsecond=0)


def decode_mac(x):
    return x


def encode_mac(x, **kwargs):
    return x


def distance(lat1, lon1, lat2, lon2):
    return haversine((lat1, lon1), (lat2, lon2))


def area_id(obj, **kwargs):
    tec = obj.cellid[0]
    opc = obj.cellid[1]
    lac = obj.cellid[2]
    return encode_cellarea(tec, opc, lac)


def cluster_networks(
    models, lookups, min_age=0, min_radius=None, min_signal=None, max_distance=None
):
    """Given a list of database models and lookups, return a list of clusters."""
    # now = util.utcnow()
    now = utcnow()  # OLS
    today = now.date()

    # Create a dict of macs mapped to their age and signal strength.
    obs_data = {}
    for lookup in lookups:
        obs_data[decode_mac(lookup.mac)] = (
            max(abs(lookup.age or min_age), 1000),
            lookup.signalStrength or min_signal,
        )

    networks = numpy.array(
        [
            (
                model.lat,
                model.lon,
                model.radius or min_radius,
                obs_data[model.mac][0],
                obs_data[model.mac][1],
                # station_score(model, now),
                model.score,  # OLS: We get the score from resolver
                encode_mac(model.mac, codec="base64"),
                bool(model.last_seen is not None and model.last_seen >= today),
            )
            for model in models
        ],
        dtype=NETWORK_DTYPE,
    )

# OLS: Allow single element clusters
#    # Only consider clusters that have at least 2 found networks
#    # inside them. Otherwise someone could use a combination of
#    # one real network and one fake and therefor not found network to
#    # get the position of the real network.
#    length = len(networks)
#    if length < 2:
#        # Not enough networks to form a valid cluster.
#        return []

    length = len(networks)
    if length == 0:
        return []
    if length == 1:
        return [networks]

    positions = networks[["lat", "lon"]]
    if length == 2:
        one = positions[0]
        two = positions[1]
        if distance(one[0], one[1], two[0], two[1]) <= max_distance:
            # Only two networks and they agree, so cluster them.
            return [networks]
        else:
            # OLS: Allow single element clusters
            return [numpy.array([networks[0]], dtype=NETWORK_DTYPE),
                    numpy.array([networks[1]], dtype=NETWORK_DTYPE)]
#            # Or they disagree forming two clusters of size one,
#            # neither of which is large enough to be returned.
#            return []

    # Calculate the condensed distance matrix based on distance in meters.
    # This avoids calculating the square form, which would calculate
    # each value twice and avoids calculating the diagonal of zeros.
    # We avoid the special cases for length < 2 with the above checks.
    # See scipy.spatial.distance.squareform and
    # https://stackoverflow.com/questions/13079563
    dist_matrix = numpy.zeros(length * (length - 1) // 2, dtype=numpy.double)
    for i, (a, b) in enumerate(itertools.combinations(positions, 2)):
        dist_matrix[i] = distance(a[0], a[1], b[0], b[1])

    link_matrix = hierarchy.linkage(dist_matrix, method="complete")
    assignments = hierarchy.fcluster(
        link_matrix, max_distance, criterion="distance", depth=2
    )

    # log.debug(f'{assignments=}')

    indexed_clusters = defaultdict(list)
    for i, net in zip(assignments, networks):
        indexed_clusters[i].append(net)

    clusters = []
    for values in indexed_clusters.values():
        # OLS: Allow single element clusters
        # if len(values) >= 2:
        #    clusters.append(numpy.array(values, dtype=NETWORK_DTYPE))
        clusters.append(numpy.array(values, dtype=NETWORK_DTYPE))

    return clusters


def cluster_cells(cells, lookups, min_age=0):
    """Cluster cells by area (lac)."""
    now = utcnow()
    today = now.date()

    # Create a dict of cell ids mapped to their age and signal strength.
    obs_data = {}
    for lookup in lookups:
        # obs_data[decode_cellid(lookup.cellid)] = (
        obs_data[lookup.cellid] = (
            max(abs(lookup.age or min_age), 1000),
            lookup.signalStrength or CELL_MIN_SIGNAL[lookup.radioType],
        )

    areas = defaultdict(list)
    for cell in cells:
        areas[area_id(cell)].append(cell)

    clusters = []
    for area_cells in areas.values():
        clusters.append(
            numpy.array(
                [
                    (
                        cell.lat,
                        cell.lon,
                        cell.radius,
                        obs_data[cell.cellid][0],  # age
                        obs_data[cell.cellid][1],  # signalStrength
                        # station_score(cell, now),
                        cell.score,
                        # encode_cellid(*cell.cellid, codec="base64"),
                        encode_cellid(*cell.cellid),
                        bool(cell.last_seen is not None and cell.last_seen >= today),
                    )
                    for cell in area_cells
                ],
                dtype=NETWORK_DTYPE,
            )
        )

    return clusters


def aggregate_mac_position(networks, minimum_accuracy):
    # Idea based on https://gis.stackexchange.com/questions/40660

    def func(point, points):
        return numpy.array(
            [
                distance(p["lat"], p["lon"], point[0], point[1])
                * min(math.sqrt(2000.0 / p["age"]), 1.0)
                / math.pow(p["signalStrength"], 2)
                for p in points
            ]
        )

    # OLS: Support single element clusters
    if len(networks) == 1:
        net = networks[0]
        lat = float(net["lat"])
        lon = float(net["lon"])
        sig = min(net["signalStrength"], WIFI_MAX_SIGNAL)
        ratio = 1.0 - abs(sig - WIFI_MIN_SIGNAL) / (WIFI_MAX_SIGNAL - WIFI_MIN_SIGNAL)
        accuracy = max(min(net["radius"] * ratio, WIFI_MAX_ACCURACY),
                       minimum_accuracy)
        return lat, lon, accuracy

    # Guess initial position as the weighted mean over all networks.
    points = numpy.array(
        [(net["lat"], net["lon"]) for net in networks], dtype=numpy.double
    )

    weights = numpy.array(
        [
            net["score"]
            * min(math.sqrt(2000.0 / net["age"]), 1.0)
            / math.pow(net["signalStrength"], 2)
            for net in networks
        ],
        dtype=numpy.double,
    )

    initial = numpy.average(points, axis=0, weights=weights)

    (lat, lon), cov_x, info, mesg, ier = leastsq(
        func, initial, args=networks, full_output=True
    )

    if ier not in (1, 2, 3, 4):
        # No solution found, use initial estimate.
        lat, lon = initial

    # Guess the accuracy as the 95th percentile of the distances
    # from the lat/lon to the positions of all networks.
    distances = numpy.array(
        [distance(lat, lon, net["lat"], net["lon"]) for net in networks],
        dtype=numpy.double,
    )
    accuracy = max(numpy.percentile(distances, 95), minimum_accuracy)

    return (float(lat), float(lon), float(accuracy))


def aggregate_cell_position(networks, min_accuracy, max_accuracy):
    """Calculate the position of the user inside the given cluster.

    Return the position, an accuracy estimate and a combined score.
    The accuracy is bounded by the min_accuracy and max_accuracy.
    """
    if len(networks) == 1:
        net = networks[0]
        lat = net["lat"]
        lon = net["lon"]
        radius = min(max(net["radius"], min_accuracy), max_accuracy)
        score = net["score"]
        return (float(lat), float(lon), float(radius), float(score))

    points = numpy.array(
        [(net["lat"], net["lon"]) for net in networks], dtype=numpy.double
    )

    weights = numpy.array(
        [
            net["score"]
            * min(math.sqrt(2000.0 / net["age"]), 1.0)
            / math.pow(net["signalStrength"], 2)
            for net in networks
        ],
        dtype=numpy.double,
    )

    lat, lon = numpy.average(points, axis=0, weights=weights)
    score = networks["score"].sum()

    # Guess the accuracy as the 95th percentile of the distances
    # from the lat/lon to the positions of all networks.
    distances = numpy.array(
        [distance(lat, lon, net["lat"], net["lon"]) for net in networks],
        dtype=numpy.double,
    )
    accuracy = min(max(numpy.percentile(distances, 95), min_accuracy), max_accuracy)

    return (float(lat), float(lon), float(accuracy), float(score))


def aggregate_cluster_position(
    cluster,
    result_type,  # OLS: Unused
    data_type,
    max_networks=None,
    min_accuracy=None,
    max_accuracy=None,
):
    """Given a single cluster, return the aggregate position inside the cluster."""
    # Sort by score, to pick the best sample of networks.
    cluster.sort(order="score")
    sample = cluster[:max_networks]

    lat, lon, accuracy = aggregate_mac_position(sample, min_accuracy)
    accuracy = min(accuracy, max_accuracy)
    score = float(cluster["score"].sum())

    used_networks = [
        # (data_type, b64decode(mac_b64), bool(seen_today))
        (data_type, netid.decode(), bool(seen_today))
        for netid, seen_today in sample[["netid", "seen_today"]]
    ]

#    return result_type(
#        lat=lat, lon=lon, accuracy=accuracy, score=score, used_networks=used_networks
#    )
    return Dummy({
        'lat': lat,
        'lon': lon,
        'accuracy': accuracy,
        'score': score,
        'used_networks': used_networks,
    })


# def best_cluster(self):
# OLS: Modified to take a list of Dummy clusters as arg
def best_cluster(input_clusters):
    """Return the best cluster from this collection."""
    # FIXME: Use signalStrength to sort single lac clusters, somehow
    if len(input_clusters) <= 1:
        return input_clusters

    # results = sorted(self, key=operator.attrgetter("accuracy"))
    results = sorted(input_clusters, key=lambda x: x.accuracy)

    clusters = {}
    for i, result1 in enumerate(results):
        clusters[i] = [result1]
        # allow a 50% buffer zone around each result
        radius1 = result1.accuracy * 1.5
        for j, result2 in enumerate(results):
            if j > i:
                # only calculate the upper triangle
                radius2 = result2.accuracy * 1.5
                max_radius = max(radius1, radius2)
                apart = distance(result1.lat, result1.lon, result2.lat, result2.lon)
                if apart <= max_radius:
                    clusters[i].append(result2)

    def sum_score(values):
        # Sort by highest cumulative score,
        # break tie by highest individual score
        return (sum([v.score for v in values]), max([v.score for v in values]))

    clusters = sorted(clusters.values(), key=sum_score, reverse=True)
    return clusters[0]


# OLS: Takes a list of Dummy clusters as arg
def best(results):
    """Return the best result in the collection."""
    _best_cluster = best_cluster(results)
    if len(_best_cluster) == 0:
        return None, ("none",)
    if len(_best_cluster) == 1:
        return _best_cluster[0], (_best_cluster[0].used_networks[0][0],)

    def best_result(result):
        # sort descending, take smallest accuracy/radius,
        # break tie by higher score
        return ((result.accuracy or 0.0), result.score * -1.0)

    sorted_results = sorted(_best_cluster, key=best_result)
    # OLS: Also return net types in best_cluster
    nettypes = {x[0] for e in sorted_results for x in e.used_networks}
    return sorted_results[0], nettypes


class ClusteringLocator(LocatorBase):
    """Clustering locator based on Mozilla Ichnaea code."""

    def __init__(self, wifiresolvers, cellresolvers, name="Unknown ClusteringLocator"):
        self.name = name
        if not (bool(wifiresolvers) or bool(cellresolvers)):
            raise RuntimeError('At least one resolver needs to be given')
        self.wifiresolvers = wifiresolvers
        self.cellresolvers = cellresolvers

    async def cluster_wifi(self, observation, hint=None):

        def found_enough(lookups):
            """Return True if there are enough (found) lookups for locating."""
            # Stop resolving after this many APs are found
            min_strongs = 3  # APs with signal > WIFI_MIN_SIGNAL
            min_total = 5

            total_found = len(lookups)
            strongs_found = len(
                [e for e in lookups if e['signalStrength'] > WIFI_MIN_SIGNAL])
            return total_found > min_total and strongs_found > min_strongs

        # A 'lookup' is observation in Ichnaea
        wifi_lookups = [{
            'mac': wob.get('macAddress'),
            'signalStrength': wob.get('signalStrength', WIFI_DEFAULT_SIGNAL),
            'age': wob.get('age', 2000),
        } for wob in observation.get('wifiAccessPoints', [])]

        found_models = []
        found_lookups = []
        for resolver in self.wifiresolvers:
            unresolved_lookups = wifi_lookups.copy()
            if not unresolved_lookups:
                break
            log.debug(f"Resolving with wifiresolver '{resolver.name}'")
            for lookup in unresolved_lookups:
                model = await resolver.resolve_wifi(lookup['mac'], hint=hint)
                if model is not None:
                    model['lat'] = model['latlon'][0]
                    model['lon'] = model['latlon'][1]
                    model['mac'] = lookup['mac']
                    model['last_seen'] = None
                    found_models.append(model)
                    found_lookups.append(lookup)
                    wifi_lookups.remove(lookup)

            if found_enough(found_lookups):
                log.debug(f"Found enough APs with wifiresolver '{resolver.name}'")
                break

        cluster_models = [Dummy(e) for e in found_models]
        cluster_lookups = [Dummy(e) for e in found_lookups]

        clusters = cluster_networks(
            cluster_models,
            cluster_lookups,
            min_radius=WIFI_MIN_ACCURACY,
            min_signal=WIFI_MIN_SIGNAL,
            max_distance=WIFI_MAX_RADIUS)

        log.debug(
            f'Wifi clusters: {len(clusters)} (' + ', '.join(
                str(len(e)) for e in clusters) + ')')

        results = []
        for cluster in clusters:
            results.append(aggregate_cluster_position(
                cluster,
                dict,  # Always dict
                "wifi",
                max_networks=WIFI_MAX_CLUSTER_SIZE,
                min_accuracy=WIFI_MIN_ACCURACY,
                max_accuracy=WIFI_MAX_ACCURACY))

        return results

    async def cluster_cell(self, observation, hint=None):
        """Resolve observations to models, cluster models, and aggregate position."""
        cell_lookups = [{
            'cellid': (cob.get('radioType', 'gsm'),
                       mcc_mnc_to_opc(cob['mobileCountryCode'],
                                      cob['mobileNetworkCode']),
                       cob['locationAreaCode'],
                       cob['cellId']),
            'signalStrength': cob.get(
                'signalStrength',
                CELL_MIN_SIGNAL[cob.get('radioType', 'gsm').upper()]),
            'age': cob.get('age', 2000),
        } for cob in observation.get('cellTowers', [])]

        found_lookups = []
        found_models = []
        for resolver in self.cellresolvers:
            unresolved_lookups = cell_lookups.copy()
            if not unresolved_lookups:
                break
            log.debug(f"Resolving with cellresolver '{resolver.name}'")
            for lookup in unresolved_lookups:
                res = await resolver.resolve_cell(*lookup['cellid'], hint=hint)
                if res is not None:
                    found_models.append(Dummy({
                        'lat': res['latlon'][0],
                        'lon': res['latlon'][1],
                        'radius': res['radius'],
                        'cellid': lookup['cellid'],
                        'score': res['score'],
                        'last_seen': None,
                    }))
                    found_lookups.append(Dummy(lookup))
                    cell_lookups.remove(lookup)
            # Resolve all cell observations, as there is usually just 1

        clusters = cluster_cells(found_models, found_lookups)

        log.debug(
            f'Cell clusters: {len(clusters)} (' + ', '.join(
                str(len(e)) for e in clusters) + ')')

        results = []
        for cluster in clusters:
            lat, lon, accuracy, score = aggregate_cell_position(
                cluster,
                min_accuracy=CELL_MIN_ACCURACY,
                max_accuracy=CELL_MAX_ACCURACY)
            used_networks = [
                ('cell', cluster[0]['netid'].decode(), cluster[0]['seen_today'])]
            results.append(Dummy({
                'lat': lat,
                'lon': lon,
                'accuracy': accuracy,
                'score': score,
                'used_networks': used_networks,
            }))

        return results

    async def locate(self, observation, hint=None, **kwargs):
        results = []
        results.extend(await self.cluster_wifi(observation, hint))
        results.extend(await self.cluster_cell(observation, hint))

        # Select the best result
        b, nets = best(results)
        if b is None:
            log.debug('No result')
            return None, None
        else:
            log.debug(
                'Result: ({:.6f}, {:.6f}), acc={:.4g}, score={:.3}, nets={}'.format(
                    b.lat, b.lon, b.accuracy, b.score, ','.join(nets)))
            return (b.lat, b.lon), b.accuracy


def run(observation):
    """Run the locator with a given observation dict (for testing)."""
    import asyncio
    import sys
    from ols.resolver.wiglenet import WiglenetResolver
    import ols.config as config

    try:
        conf = config.get_config()
    except config.ConfigError as e:
        print(str(e))
        sys.exit(1)
    except FileNotFoundError as e:
        print('Could not load config: ' + str(e))
        sys.exit(1)

    timestampformat = '[%(asctime)s.%(msecs)03d] ' if conf['timestamps'] else ''
    logging.basicConfig(
        level=logging.WARNING,  # Loglevel for library modules
        format=timestampformat + '%(module)s %(levelname)s: %(message)s',
        datefmt='%Y-%m-%d %H:%M:%S')
    log = logging.getLogger(__name__)
    # logging.getLogger('ols').setLevel(conf['loglevel'])
    logging.getLogger('ols').setLevel(logging.DEBUG)

    log.debug('Starting clustering locator test')

    method_conf = conf['resolver']['wigle']

    resolver = WiglenetResolver(
        method_conf['apiuser'], method_conf['apitoken'], method_conf['db'])

    locator = ClusteringLocator(resolver, resolver)
    asyncio.run(locator.locate(observation))
