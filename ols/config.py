import argparse
import logging
import os
import sys
import tomllib

from . import __version__

description = 'Offline Location Service'

DEFAULT_DATADIR = os.path.expanduser(os.path.join(
    os.environ.get('XDG_DATA_HOME', '~/.local/share'), 'ols'))
DEFAULT_CONFFILE = os.path.expanduser(os.path.join(
    os.environ.get('XDG_CONFIG_HOME', '~/.config'), 'ols/ols.toml'))
DEFAULT_LOGLEVEL = 'warning'

loglevels = {
    'critical': logging.CRITICAL,
    'error': logging.ERROR,
    'warning': logging.WARNING,
    'info': logging.INFO,
    'debug': logging.DEBUG
}

# List of (args, kwargs) tuples for ArgumentParser.add_argument
# Command line args not in config file
cmdline_args = [
    (('-v', '--version'), {
        'dest': 'version',
        'action': 'store_true',
        'help': "Print version number and exit",
    }),
    (('-C', '--config-file'), {
        'dest': 'configfile',
        'action': 'store',
        'default': DEFAULT_CONFFILE,
        'help': f"Configuration (TOML) file, default '{DEFAULT_CONFFILE}'",
    }),
]

# Args both in command line and config file
conffile_args = [
    (('-p', '--port'), {
        'dest': 'port',
        'action': 'store',
        'default': 8088,
        'help': 'Server port',
    }),
    (('-H', '--host'), {
        'dest': 'host',
        'action': 'store',
        'default': '127.0.0.1',
        'help': 'Server host',
    }),
    # (('-l', '--logfile'), {
    #     'dest': 'logfile',
    #     'action': 'store',
    #     'default': 'ols.log',
    #     'help': 'Log file',
    # }),
    (('-o', '--obsdb'), {
        'dest': 'obsdb',
        'action': 'store',
        'default': None,
        'help': 'Location of the observation database file',
    }),
    (('-D', '--datadir'), {
        'dest': 'datadir',
        'action': 'store',
        'default': DEFAULT_DATADIR,
        'help': f"Base directory for data files, default '{DEFAULT_DATADIR}'",
    }),
    (('-d', '--debug'), {
        'dest': 'debuglevel',
        'action': 'store',
        'choices': loglevels.keys(),
        'default': DEFAULT_LOGLEVEL,
        'help': f"Set logging level, default '{DEFAULT_LOGLEVEL}'",
    }),
    (('--timestamps',), {
        'dest': 'timestamps',
        'action': argparse.BooleanOptionalAction,
        'help': 'Print debug log messages with timestamps, or not. '
                'Default is to print timestamps, except when running under systemd.',
    }),
]

valid_locator_types = [
    'clustering',
    'firstcell',
    'm8b',
    'strongestcell',
    'strongestwifi',
    'web'
]
valid_resolver_types = ['cellid', 'wiglenet', 'wiglenetcache', 'localdb']


class ConfigError(Exception):
    pass


def parse_args():
    """Return cmdline config in a dict."""
    parser = argparse.ArgumentParser(description=description)
    for args, kwargs in cmdline_args + conffile_args:
        parser.add_argument(*args, **kwargs)
    return vars(parser.parse_args())


def parse_configfile(configfilename):
    with open(configfilename, 'r') as c:
        return tomllib.loads(c.read())


def get_config():
    defaults = {d['dest']: d.get('default')
                for _, d in cmdline_args + conffile_args}
    argconf = parse_args()
    if argconf.get('version', False):
        print(__version__)
        sys.exit(1)

    fileconf = parse_configfile(argconf.get('configfile'))
    conf = dict(defaults)
    conf.update(fileconf)
    conf.update((k, v) for k, v in argconf.items() if v != defaults[k])

    # Cleanups and checks

    # Convert 'debuglevel' to 'loglevel'
    conf['loglevel'] = loglevels[conf['debuglevel']]

    # Debug log timestamps
    if conf['timestamps'] is None:
        # INVOCATION_ID is defined when running as systemd unit
        systemd = 'INVOCATION_ID' in os.environ
        conf['timestamps'] = not systemd

    conf['locator'] = conf.get('locator', {})
    for k, v in conf['locator'].items():
        if not isinstance(v, dict):
            conf['locator'].pop(k)

    conf['resolver'] = conf.get('resolver', {})
    for k, v in conf['resolver'].items():
        if not isinstance(v, dict):
            conf['resolver'].pop(k)

    for resolver in conf['resolver'].values():
        rtype = resolver.get('type')
        if rtype is None or rtype not in valid_resolver_types:
            raise ConfigError(f"Resolver type {rtype} is not valid")

    for locator in conf['locator'].values():
        ltype = locator.get('type')
        if ltype is None or ltype not in valid_locator_types:
            raise ConfigError(f"Locator type {ltype} is not valid")
        for resolver in locator.get('wifiresolvers', []):
            if resolver not in conf['resolver']:
                raise ConfigError(
                    f"Wifi resolver '{resolver}' in "
                    f"locator '{locator}' not found in config")
        for resolver in locator.get('cellresolvers', []):
            if resolver not in conf['resolver']:
                raise ConfigError(
                    f"Cell resolver '{resolver}' in "
                    f"locator '{locator}' not found in config")

    methods = conf.get('methods')
    if methods is None:
        raise ConfigError("'methods' variable not defined in config")

    if not isinstance(methods, list):
        raise ConfigError(
            "The 'methods' config var must be a list of lists with locator names")

    for locator_group in methods:
        if not isinstance(locator_group, list):
            raise ConfigError(
                "The 'methods' config var must be a list of lists with locator names")
        for locator in locator_group:
            if locator not in conf['locator'].keys():
                raise ConfigError(
                    f"Locator '{locator}' in methods is not in configured locators")

    if isinstance(conf['datadir'], str):
        if conf['datadir'].startswith('~'):
            conf['datadir'] = os.path.expanduser(conf['datadir'])
        if not os.access(conf['datadir'], os.W_OK):
            if conf['datadir'] == DEFAULT_DATADIR:
                os.makedirs(DEFAULT_DATADIR)
            else:
                raise ConfigError(
                    f"Datadir '{conf['datadir']}' is not writable")
    else:
        raise ConfigError('Datadir must be defined')

    datadir = conf['datadir']

    def fix_datapath(confd, fkey):
        if confd.get(fkey):
            confd[fkey] = expand_filename(confd[fkey], datadir)

    fix_datapath(conf, 'obsdb')

    for wconf in (b for b in conf.get('resolver', {}).values()
                  if b.get('type', '') in ('wiglenet', 'cellid', 'localdb')):
        fix_datapath(wconf, 'db')

    for wconf in (b for b in conf.get('locator', {}).values()
                  if b.get('type', '') in ('m8b',)):
        fix_datapath(wconf, 'datafile')

    # Store updatable localdb files
    localdb_files = []
    for wconf in (b for b in conf.get('resolver', {}).values()
                  if b.get('type', '') in ('localdb',)):
        if wconf.get('update', False):
            dbfile = wconf.get('db')
            if dbfile is not None:
                localdb_files.append(dbfile)

    conf['localdb_files'] = localdb_files

    return conf


def expand_filename(fname, basedir=None):
    if fname.startswith(('/', './')):
        return fname
    elif fname.startswith('~'):
        return os.path.expanduser(fname)
    elif basedir is not None:
        return os.path.join(basedir, fname)

    return fname
