import logging
import sqlite3
import sys
from datetime import datetime
from typing import Optional

from .utils import mcc_mnc_to_opc

log = logging.getLogger(__name__)


class UpdaterBase(object):
    """Base class for LocalDB updater."""

    def insert_locate(self,
                      observation: dict,
                      latlon: Optional[tuple[float, float]],
                      accuracy: Optional[float]) -> None:
        return None

    def insert_submit(self, submission: dict) -> None:
        return None


class ObservationDBBase(UpdaterBase):
    """A no-op obsdb. Defines the API and can be used as dummy DB."""

    def __init__(self, filename):
        pass

    def get_observation(self, timestamp):
        return {
            'wifis': [],
            'cells': [],
        }

    def get_observation_list(self):
        return []

    def remove(self, timestamp):
        pass

    def close(self):
        pass


class ObservationDB(ObservationDBBase):
    location_sources = {
        'No location': 0,
        'GNSS': 1,
        'Manual': 2,
        'Fused': 3,
        'Mozilla': 10,
        'Google': 11,
        'Yandex': 12,
        'OLS': 13,
        'Other': 99,
    }

    radiotypes = {
        'cdma': 1,
        'gsm': 2,
        'wcdma': 3,
        'lte': 4,
        'nr': 5,
    }

    def __init__(self, filename):
        self.con = sqlite3.connect(filename)
        self.con.row_factory = sqlite3.Row
        self.maybe_create()

    def maybe_create(self):
        cur = self.con.cursor()

        cur.execute("""
            CREATE TABLE IF NOT EXISTS observations (
                timestamp INTEGER,
                lat REAL,
                lon REAL,
                locsource INTEGER,
                accuracy REAL,
                PRIMARY KEY (timestamp)
            );
        """)

        cur.execute("""
            CREATE TABLE IF NOT EXISTS wifi (
                timestamp INTEGER,
                mac TEXT,
                strength REAL,
                PRIMARY KEY (timestamp, mac)
            );
        """)
        cur.execute("""
            CREATE INDEX IF NOT EXISTS idx_wifi_mac ON wifi(mac);
        """)

        cur.execute("""
            CREATE TABLE IF NOT EXISTS cell (
                timestamp INTEGER,
                tec INTEGER,
                opc TEXT,
                lac INTEGER,
                cid INTEGER,
                strength REAL,
                PRIMARY KEY (timestamp, tec, opc, lac, cid)
            );
        """)
        cur.execute("""
            CREATE INDEX IF NOT EXISTS idx_cell_id ON cell(tec, opc, lac, cid);
        """)

        cur.execute("""
            CREATE TABLE IF NOT EXISTS bt (
                timestamp INTEGER,
                mac TEXT,
                strength REAL,
                PRIMARY KEY (timestamp, mac)
            );
        """)
        cur.execute("""
            CREATE INDEX IF NOT EXISTS idx_bt_mac ON bt(mac);
        """)

    def _insert_observation(self,
                            observation: dict,
                            timestamp: int,
                            latlon: Optional[tuple[float, float]],
                            accuracy: Optional[float],
                            source: int) -> None:
        if latlon is None:
            lat, lon = None, None
        else:
            lat, lon = latlon

        cur = self.con.cursor()
        cur.execute("""
            INSERT OR REPLACE INTO observations VALUES(?, ?, ?, ?, ?)
        """, (timestamp, lat, lon, source, accuracy))

        for ap in observation.get('wifiAccessPoints', []):
            vals = {
                'timestamp': timestamp,
                'mac': ap['macAddress'],
                'strength': ap.get('signalStrength'),
            }
            cur.execute("""
                INSERT OR REPLACE INTO wifi VALUES(
                    :timestamp,
                    :mac,
                    :strength
                )
            """, vals)

        for ap in observation.get('bluetoothBeacons', []):
            vals = {
                'timestamp': timestamp,
                'mac': ap['macAddress'],
                'strength': ap.get('signalStrength')
            }
            cur.execute("""
                INSERT OR REPLACE INTO bt VALUES(
                    :timestamp,
                    :mac,
                    :strength
                )
            """, vals)

        for ap in observation.get('cellTowers', []):
            vals = {
                'timestamp': timestamp,
                'tec': self.radiotypes.get(ap['radioType']),
                'opc': mcc_mnc_to_opc(ap['mobileCountryCode'], ap['mobileNetworkCode']),
                'lac': ap['locationAreaCode'],
                'cid': ap['cellId'],
                'strength': ap.get('signalStrength')
            }
            cur.execute("""
                INSERT OR REPLACE INTO cell VALUES(
                    :timestamp,
                    :tec,
                    :opc,
                    :lac,
                    :cid,
                    :strength
                )
            """, vals)

        self.con.commit()

    def _have_obs(self, timestamp):
        cur = self.con.cursor()
        have_obs = cur.execute("""
            SELECT EXISTS(SELECT * FROM observations WHERE timestamp = ?);
            """, (timestamp,)).fetchone()[0]
        return have_obs

    def insert_locate(self, observation, latlon, accuracy):
        timestamp = int(observation['time'].timestamp() * 1e6)  # us from epoch
        if self._have_obs(timestamp):
            log.warning('Observation with duplicate timestamp, not inserting')
            return

        locsource = (self.location_sources['OLS']
                     if latlon is not None else self.location_sources['No location'])
        self._insert_observation(
            observation, timestamp, latlon, accuracy, locsource)

    def insert_submit(self, submission):
        mls_source_to_locsource = {
            'gps': 'GNSS',
            'manual': 'Manual',
            'fused': 'Fused',
        }
        for item in submission['items']:
            # Submission timestamp is in ms since epoch, we use us since epoch
            timestamp = item.get(
                'timestamp', int(datetime.now().timestamp()) * 1e3) * 1e3
            if self._have_obs(timestamp):
                log.warning('Submission with duplicate timestamp, not inserting')
                continue

            pos = item.get('position')
            if pos and pos.get('latitude') and pos.get('longitude'):
                latlon = (pos['latitude'], pos['longitude'])
                accuracy = pos.get('accuracy')
                source = self.location_sources[
                    mls_source_to_locsource[item.get('source', 'gps')]]
            else:
                latlon = (None, None)
                accuracy = None
                source = self.location_sources['No location']

            self._insert_observation(item, timestamp, latlon, accuracy, source)

    def get_observation(self, timestamp):
        """Return single observation as a dict.

        The return value is a dict with observation table columns as keys.
        The 'wifi' and 'cell' keys contain a list of wifi and cell observations
        belonging with the given timestamp.
        """
        cur = self.con.cursor()
        row = cur.execute("""
            SELECT * FROM observations WHERE timestamp = ?;
            """, (timestamp,))
        obs = row.fetchone()
        obsd = {k: obs[k] for k in obs.keys()}
        wifis = cur.execute("""
            SELECT mac, strength FROM wifi WHERE timestamp = ?;
            """, (timestamp,))
        wlist = [{k: w[k] for k in w.keys()} for w in wifis]

        cells = cur.execute("""
            SELECT tec, opc, lac, cid, strength FROM cell WHERE timestamp = ?;
            """, (timestamp,))
        clist = [{k: c[k] for k in c.keys()} for c in cells]

        obsd['wifis'] = wlist
        obsd['cells'] = clist

        return obsd

    def get_observation_list(self):
        """Return a list of all timestamps (= observation keys) in the database."""
        cur = self.con.cursor()
        timestamps = cur.execute("""
            SELECT timestamp FROM observations ORDER BY timestamp;
            """)
        return [t[0] for t in timestamps.fetchall()]

    def _get_positions(self):
        """Return a list of all rows in observation table."""
        cur = self.con.cursor()
        locations = cur.execute("""
            SELECT * FROM observations;
            """).fetchall()
        return [{k: loc[k] for k in loc.keys()} for loc in locations]

    def remove(self, timestamp):
        """Remove an observation and its associated wifi, cell and bt rows."""
        cur = self.con.cursor()
        cur.execute("""
            DELETE FROM observations WHERE timestamp = ?;
            """, (timestamp,))
        cur.execute("""
            DELETE FROM wifi WHERE timestamp = ?;
            """, (timestamp,))
        cur.execute("""
            DELETE FROM cell WHERE timestamp = ?;
            """, (timestamp,))
        cur.execute("""
            DELETE FROM bt WHERE timestamp = ?;
            """, (timestamp,))
        self.con.commit()


def main(obfilename, dbfilename):
    with open(obfilename, 'r') as fin:
        import yaml
        observations = yaml.safe_load(fin.read())

    obsdb = ObservationDB(dbfilename)

    for obs in observations:
        obsdb.insert_locate(obs)


if __name__ == '__main__':
    main(sys.argv[1], sys.argv[2])
