import setuptools


def get_version(rel_path):
    with open("./" + rel_path, 'r') as fp:
        for line in fp.readlines():
            if line.startswith('__version__'):
                delim = '"' if '"' in line else "'"
                return line.split(delim)[1]
        else:
            raise RuntimeError("Unable to find version string.")


setuptools.setup(
    name="ols",
    version=get_version('ols/__init__.py'),
    author="Teemu Ikonen",
    author_email="tpikonen@mailbox.org",
    description="Offline Location Service",
    license="AGPL3",
    long_description="""
# Offline Location Service

A local HTTP server implementing the Mozilla Location Service (MLS) API.
Uses multiple data sources and device-local offline or caching databases.

Mainly intended to be used as an alternative web location source for GeoClue.
""",
    long_description_content_type="text/markdown",
    url="https://codeberg.org/tpikonen/ols",
    platforms=["Linux"],
    project_urls={
        "Bug Tracker": "https://codeberg.org/tpikonen/ols/issues",
    },
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Environment :: No Input/Output (Daemon)",
        "Framework :: AsyncIO",
        "Intended Audience :: End Users/Desktop",
        "License :: OSI Approved :: GNU Affero General Public License v3",
        "Operating System :: POSIX :: Linux",
        "Programming Language :: Python :: 3.11",
        "Topic :: Scientific/Engineering :: GIS",
    ],
    package_dir={"": "./"},
    packages=setuptools.find_packages(where="./"),
    package_data={
        "ols.data": ["ols-example-conf.toml", "ols-example-user.service"],
    },
    python_requires=">=3.11",
    install_requires=[
        "aiohttp",
        "fastjsonschema",
        "mercantile",
        "msgpack",
        "numpy",
        "packaging",
        "scipy",
    ],
    zip_safe=False,
    entry_points={
        "console_scripts": [
            "ols=ols.server:main",
            "cellid-ols-import=ols.resolver.cellid.cellid_import:main",
            "m8b-to-msgpack=ols.m8bfile:main",
        ]
    }
)
