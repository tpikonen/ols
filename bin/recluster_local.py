#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Entry point script for running ols inside the source tree
import re
import sys
from pathlib import Path

# Run from local source dir
sys.path.insert(0, str(Path(sys.argv[0]).resolve().parent.parent))

from ols.resolver.local import recluster_main  # noqa: E402
if __name__ == '__main__':
    sys.argv[0] = re.sub(r'(-script\.pyw|\.exe)?$', '', sys.argv[0])
    sys.exit(recluster_main())
