import asyncio
import copy
import logging
import sys
import unittest
from collections.abc import Iterable

from numpy.testing import assert_allclose

from ols.constants import CELL_MIN_SIGNAL
from ols.locator.clustering import ClusteringLocator
from ols.resolver.dictresolver import DictResolver
from ols.utils import encode_cellid, encode_wifi, haversine

timestampformat = '[%(asctime)s.%(msecs)03d] '
logging.basicConfig(
    level=logging.WARNING,  # Loglevel for library modules
    format=timestampformat + '%(module)s %(levelname)s: %(message)s',
    datefmt='%Y-%m-%d %H:%M:%S')
log = logging.getLogger(__name__)
# logging.getLogger('ols').setLevel(logging.DEBUG)
logging.getLogger('ols').setLevel(logging.CRITICAL)

celldb_1 = {
    encode_cellid("GSM", "00101", 1, 1): {
        'latlon': (0.001, 0.001),
        'radius': 1000,
        'age': 10 * 24 * 60 * 60 * 1e6,  # 10 days
        'score': 1,
    }
}

celldb_2cid = celldb_1.copy()
celldb_2cid.update({
    encode_cellid("GSM", "00101", 1, 2): {
        'latlon': (-0.001, -0.001),
        'radius': 1000,
        'age': 10 * 24 * 60 * 60 * 1e6,  # 10 days
        'score': 1,
    }
})

celldb_2lac = celldb_1.copy()
celldb_2lac.update({
    encode_cellid("GSM", "00101", 2, 1): {
        'latlon': (-0.001, -0.001),
        'radius': 1000,
        'age': 10 * 24 * 60 * 60 * 1e6,  # 10 days
        'score': 1,
    }
})

cellobs_1 = {
    'cellTowers': [
        {
            'mobileCountryCode': 1,
            'mobileNetworkCode': 1,
            'locationAreaCode': 1,
            'cellId': 1,
        },
    ]
}

cellobs_2cid = copy.deepcopy(cellobs_1)
cellobs_2cid['cellTowers'].append({
    'mobileCountryCode': 1,
    'mobileNetworkCode': 1,
    'locationAreaCode': 1,
    'cellId': 2,
})

cellobs_2lac = copy.deepcopy(cellobs_1)
cellobs_2lac['cellTowers'].append({
    'mobileCountryCode': 1,
    'mobileNetworkCode': 1,
    'locationAreaCode': 2,
    'cellId': 1,
    'signalStrength': CELL_MIN_SIGNAL['GSM'] + 50,
})

wifiobs_1 = {
    'wifiAccessPoints': [
        {
            'macAddress': '00:00:00:00:00:01',
        },
    ]
}

wifiobs_1s = {
    'wifiAccessPoints': [
        {
            'macAddress': '00:00:00:00:00:01',
            'signalStrength': -50,
        },
    ]
}

wifiobs_1a = {
    'wifiAccessPoints': [
        {
            'macAddress': '00:00:00:00:00:01',
            'age': 30,
        },
    ]
}

wifiobs_2 = copy.deepcopy(wifiobs_1)
wifiobs_2['wifiAccessPoints'].append({
    'macAddress': '00:00:00:00:00:02',
})


wifiobs_2s = {
    'wifiAccessPoints': [
        {
            'macAddress': '00:00:00:00:00:01',
            'signalStrength': -40,
        },
        {
            'macAddress': '00:00:00:00:00:02',
            'signalStrength': -50,
        },
    ]
}

wifiobs_2a = {
    'wifiAccessPoints': [
        {
            'macAddress': '00:00:00:00:00:01',
            'age': 2000,
        },
        {
            'macAddress': '00:00:00:00:00:02',
            'age': 4000,
        },
    ]
}

wifiobs_2s2 = {
    'wifiAccessPoints': [
        {
            'macAddress': '00:00:00:00:00:01',
            'signalStrength': -50,
        },
        {
            'macAddress': '00:00:00:00:00:02',
            'signalStrength': -40,
        },
    ]
}

wifiobs_3 = copy.deepcopy(wifiobs_2)
wifiobs_3['wifiAccessPoints'].append({
    'macAddress': '00:00:00:00:00:03',
})

wifiobs_3s = copy.deepcopy(wifiobs_2s)
wifiobs_3s['wifiAccessPoints'].append({
    'macAddress': '00:00:00:00:00:03',
    'signalStrength': -50,
})

wifiobs_3s2 = copy.deepcopy(wifiobs_2s2)
wifiobs_3s2['wifiAccessPoints'].append({
    'macAddress': '00:00:00:00:00:03',
    'signalStrength': -30,
})


def flatten(x, *, max_depth=sys.maxsize):
    """Return a list from an iterable with contained iterables flattened.

    If x is not iterable, put it on a single-element list.
    Optionally flatten only to max_depth recursions.
    """
    out = []

    def _flatten(it, depth):
        for elem in it:
            if isinstance(elem, Iterable) and depth > 0:
                _flatten(elem, depth - 1)
            else:
                out.append(elem)

    if not isinstance(x, Iterable):
        return [x]

    _flatten(x, max_depth)
    return out


def assert_loc_close(x, y, **kwargs):
    assert_allclose(flatten(x), flatten(y), **kwargs)


class TestClusteringLocator(unittest.TestCase):

    def test_no_resolvers(self):
        self.assertRaises(RuntimeError, ClusteringLocator, [], [])

    def test_no_obs(self):
        cl = ClusteringLocator([], [DictResolver(celldb_1)])
        location = asyncio.run(cl.locate({}))
        self.assertEqual(location, (None, None))

    def test_cell_one(self):
        cl = ClusteringLocator([], [DictResolver(celldb_1)])
        location = asyncio.run(cl.locate(cellobs_1))
        assert_loc_close(location, ((0.001, 0.001), 1000))
        location = asyncio.run(cl.locate(cellobs_2cid))
        assert_loc_close(location, ((0.001, 0.001), 1000))
        location = asyncio.run(cl.locate(cellobs_2lac))
        assert_loc_close(location, ((0.001, 0.001), 1000))

    def test_cell_2cid(self):
        cl = ClusteringLocator([], [DictResolver(celldb_2cid)])
        location = asyncio.run(cl.locate(cellobs_2cid))
        assert_loc_close(location, ((0.000, 0.000), 1000))
        location = asyncio.run(cl.locate(cellobs_2lac))
        assert_loc_close(location, ((0.001, 0.001), 1000))

    def test_cell_2lac(self):
        cl = ClusteringLocator([], [DictResolver(celldb_2lac)])
        location = asyncio.run(cl.locate(cellobs_2cid))
        assert_loc_close(location, ((0.001, 0.001), 1000))
        location = asyncio.run(cl.locate(cellobs_2lac))
        # This should be ((-0.001, -0.001), 1000).
        # Ichnaea should pick the lac with larger signalStrength, but doesn't.
        assert_loc_close(location, ((0.001, 0.001), 1000))

    def test_wifi_1obs(self):
        wifidb_1 = {
            encode_wifi('00:00:00:00:00:01'): {
                'latlon': (0.001, 0.001),
                'radius': 100,
                'age': 10 * 24 * 60 * 60 * 1e6,
                'score': 1,
            }
        }
        cl = ClusteringLocator([DictResolver(wifidb_1)], [])
        location = asyncio.run(cl.locate(wifiobs_1))
        assert_loc_close(location, ((0.001, 0.001), 87.5))
        location = asyncio.run(cl.locate(wifiobs_1s))
        assert_loc_close(location, ((0.001, 0.001), 50))
        location = asyncio.run(cl.locate(wifiobs_1a))
        assert_loc_close(location, ((0.001, 0.001), 87.5))

    def test_wifi_2obs(self):
        wifidb_2 = {
            encode_wifi('00:00:00:00:00:01'): {
                'latlon': (0.001, 0.001),
                'radius': 100,
                'age': 10 * 24 * 60 * 60 * 1e6,
                'score': 1,
            },
            encode_wifi('00:00:00:00:00:02'): {
                'latlon': (-0.001, -0.001),
                'radius': 100,
                'age': 10 * 24 * 60 * 60 * 1e6,
                'score': 1,
            }
        }
        cl = ClusteringLocator([DictResolver(wifidb_2)], [])
        location = asyncio.run(cl.locate(wifiobs_1))
        assert_loc_close(location, ((0.001, 0.001), 87.5))
        location = asyncio.run(cl.locate(wifiobs_2))
        assert_loc_close(location, ((0.0, 0.0), haversine((0.001, 0.001), (0.0, 0.0))))
        location = asyncio.run(cl.locate(wifiobs_2s))
        assert_loc_close(
            location,
            ((0.0004188371378499965, 0.00041883771997240095), 216.53061203857754))
        location = asyncio.run(cl.locate(wifiobs_2a))
        assert_loc_close(
            location,
            ((0.00033333335602586873, 0.00033333335602586873), 204.42938853366638))

    def test_wifi_1clus(self):
        wifidb_3 = {
            encode_wifi('00:00:00:00:00:01'): {
                'latlon': (0.0, 0.0),
                'radius': 100,
                'age': 10 * 24 * 60 * 60 * 1e6,
                'score': 1,
            },
            # AP distance just below WIFI_MAX_RADIUS = 500.0 m
            encode_wifi('00:00:00:00:00:02'): {
                'latlon': (0.004, 0.0),
                'radius': 100,
                'age': 10 * 24 * 60 * 60 * 1e6,
                'score': 1,
            },
        }
        cl = ClusteringLocator([DictResolver(wifidb_3)], [])
        location = asyncio.run(cl.locate(wifiobs_2))
        assert_loc_close(location, ((0.002, 0.0), 222.3898532891175))
        location = asyncio.run(cl.locate(wifiobs_2s))
        assert_loc_close(location, ((0.0015609756097560976, 0.0), 266.3254096706261))
        location = asyncio.run(cl.locate(wifiobs_2a))
        assert_loc_close(location, ((0.0016568542494923802, 0.0), 256.7303131895956))
        location = asyncio.run(cl.locate(wifiobs_2s2))
        assert_loc_close(location, ((0.0024390243902439024, 0.0), 266.3254096706261))

    def test_wifi_2clus(self):
        wifidb_4 = {
            # AP distance just above WIFI_MAX_RADIUS = 500.0 m
            encode_wifi('00:00:00:00:00:01'): {
                'latlon': (0.0, 0.0),
                'radius': 100,
                'age': 10 * 24 * 60 * 60 * 1e6,  # 10 days
                'score': 1,
            },
            encode_wifi('00:00:00:00:00:02'): {
                'latlon': (0.0045, 0.0),
                'radius': 100,
                'age': 10 * 24 * 60 * 60 * 1e6,  # 10 days
                'score': 1,
            },
        }
        cl = ClusteringLocator([DictResolver(wifidb_4)], [])
        location = asyncio.run(cl.locate(wifiobs_2))
        assert_loc_close(location, ((0.0, 0.0), 87.5))
        location = asyncio.run(cl.locate(wifiobs_2s))
        assert_loc_close(location, ((0.0, 0.0), 37.5))
        location = asyncio.run(cl.locate(wifiobs_2s))
        assert_loc_close(location, ((0.0, 0.0), 37.5))
        location = asyncio.run(cl.locate(wifiobs_2a))
        assert_loc_close(location, ((0.0, 0.0), 87.5))
        location = asyncio.run(cl.locate(wifiobs_2s2))
        assert_loc_close(location, ((0.0045, 0.0), 37.5))

    def test_wifi_3clus(self):
        wifidb_4 = {
            # AP distances just above WIFI_MAX_RADIUS = 500.0 m
            encode_wifi('00:00:00:00:00:01'): {
                'latlon': (0.0, 0.0),
                'radius': 100,
                'age': 10 * 24 * 60 * 60 * 1e6,  # 10 days
                'score': 1,
            },
            encode_wifi('00:00:00:00:00:02'): {
                'latlon': (0.0045, 0.0),
                'radius': 100,
                'age': 10 * 24 * 60 * 60 * 1e6,  # 10 days
                'score': 1,
            },
            encode_wifi('00:00:00:00:00:03'): {
                'latlon': (0.0, 0.0045),
                'radius': 100,
                'age': 10 * 24 * 60 * 60 * 1e6,  # 10 days
                'score': 1,
            },
        }
        cl = ClusteringLocator([DictResolver(wifidb_4)], [])
        location = asyncio.run(cl.locate(wifiobs_3))
        assert_loc_close(location, ((0.0, 0.0), 87.5))
        location = asyncio.run(cl.locate(wifiobs_3s))
        assert_loc_close(location, ((0.0, 0.0), 37.5))
        location = asyncio.run(cl.locate(wifiobs_3s2))
        assert_loc_close(location, ((0.0, 0.0045), 25.0))


if __name__ == '__main__':
    unittest.main()
