# OLS 0.2.0 (2024-01-04)

## Breaking changes

 * Rename -T / --logtimestamps command line option to --timestamps /
   --no-timestamps, and the configuration variable 'logtimestamps' to
   'timestamps'. The default is to have timestamps enabled, unless running as a
   systemd unit.
 * The 'apikey' configuration variable is removed from the web source config.
   API key can be set in the URL with the 'key' URL parameter
 * The 'methods' configuration variable is now a list of locator groups.
   A locator group is a list of locators. Locator groups are run sequentially,
   until a location is found. All locators inside a group are run concurrently
   and if results are found, the one with the best accuracy is returned. The
   example configuration file has more details.
 * The -m / --method command line option has been removed, the location methods
   must be set in the configuration file.

## New features

 * 'localdb' resolver: Stores AP and cell tower observations received via the
   Ichnaea submission API to a local sqlite database and clusters the
   observations to usable station models. Optionally ('update' config var)
   stores also observations from all successful locate API calls.
 * Added code to convert opencellid CSV files to sqlite DB used by the cellid
   resolver
 * The latest m8b file release from wigle.net improves the performance of the
   m8b locator significantly, so it's now considered fully operational. The MGRS
   squares from m8b are converted into lat,lon coordinates of the square center.
 * Added m8b file reader module ols.m8bfile and an m8b to msgpack conversion
   function
 * New entry point scripts included in distribution (and under bin/ in sources):
   - cellid-ols-import: Import CSV files to a sqlite DB for cellid resolver
   - m8b-to-msgpack: Extract data subsets from an m8b file to msgpack files used
     by the m8b locator
 * Added -v / --version command line option

## Contributors

 * Teemu Ikonen
 * Sebastian Krzyszkowiak

# OLS 0.1.0 (2023-03-24)

 * First release
 * Location server based on aiotthp with an Ichnaea compatible API
 * Resolver (netid to AP coordinates and range) implementations:
  - wigle.net API with caching
  - Offline cell ID database
 * Locator (AP observations to location and accuracy) implementations:
  - Web, AKA passtrough to an Ichnaea compatible server
  - Clustering locator inspired by Ichnaea, combines multiple resolved APs to a
    location and an accuracy estimate
  - Sigle locator which returns the coordinates of the AP with the strongest
    signal in the observation and a signal strength based accuracy estimate.
    Mostly for testing.
  - Low-resolution offline m8b locator using the 'magic 8 ball' data files
    released by wigle.net. Does not work well due to limitations in data and
    upstream bugs.
 * Observation database which (if enabled) records all observations submitted to
   the server.

Contributors:

 * Teemu Ikonen
